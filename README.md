VR Keyboard
===========

A Godot based keyboard built of 3D objects that can be triggered by either 
mouse clicks, collisions with Area or RigidBody, or VR pointer clicking.

![Keyboard image](icon.png)

Usage
-----
Load `Scenes/KeyboardDemo.tscn` to see it in action.

To use elsewhere:
 - Simply drag the `VRKeyboard/Keyboard.tscn` into your scene
 - Specify a json layout file, for instance one from `VRKeyboard/Layouts`
 - Hit the `Build Layout` toggle in the Inspector
 - Connect KeyPressed signals from Keyboard to your own scripts
 - Done!

If the key labels get out of sync after you build, you can hit "Refresh Layout"
in the inspector panel. For some reason, modifications made by tool scripts
to some instanced nested nodes are not always preserved.

Please note this is not currently a full input method system. For more complicated
input methods such as for Chinese, or right to left such as Arabic, a lot of work needs to be done.


Changing Keyboards
------------------

To change the keyboard layout, just set another layout file, and
toggle `Build Layouts` again.

For instance, this is a number pad:

	{
		"name": "Combination Pad",

		"models": [
			"res://VRKeyboard/KeyModels/Key.tscn",
			"res://VRKeyboard/KeyModels/Key2.tscn",
			"res://VRKeyboard/KeyModels/Key3.tscn"
		],
		
		"rows": [
			"7 8 9",
			"4 5 6",
			"1 2 3",
			"Bksp 0 Enter"
		],

		"specials": {
			"Bksp":  { "model": 0, "cap": "*", "value": "\b" },
			"Enter": { "model": 0, "cap": "#", "value": "\n", "icon":"res://VRKeyboard/Icons/Enter.png" },
		},

		"row_offsets": [ 0,0,0,0 ]
	}

- "models" is a list of key models. Any key can point to any of the models.
  You can make any key models you want, as long as they derive from KVR_Key.

- "rows" is either a space separated list of keys on the row, or an array of keys.
  If one of the pieces matches anything in "specials", then that "specials" info 
  gives special settings of the key. If a piece is not in "specials", then each character
  of the piece corresponds to different modifiers. For instance, "1!" would mean
  "1" is returned normally, and "!" is returned when shift is active.

- "row_offsets": an array of horizontal displacements for the rows

- "specials" is a dictionary of keys that have special behavior. Each special can have:
   - "model": index into the models array
   - "cap" : what text should appear on the key
   - "value": what text should be sent when a key is pressed. Use '\b' for backspace.
   - "icon": use an icon instead of a text label
   - "offsetx": extra horizontal offset to apply to key location
   - "offsety": extra vertical (z direction in godot space) offset to apply to key location
   - "key": special id for the key. No text is actually meant to be sent with the key
   - "mod_number": number for a modifier mask. Key will behave like a toggle and change key labels and values.



VR Notes
--------
While the keyboard itself is not dependent to any particular VR system, this demo was built using a 
Vive and Linux, so you might have problems if you are using other things!

To adapt to other VR control systems, the important thing to know is that a key click is triggered
on Area or RigidBody collisions from objects that share a collision layer with the keys. These collisions
emit signals to the parent Keyboard object which does some preprocessing related to key modifiers,
and then emits signals to your custom code. For point and click, each key 
has `_on_pointer_pressed()`, `_on_pointer_released()`, `_on_pointer_hover_start()`, and `_on_pointer_hover_end()`
methods in the parent of the colliding Area, which your VR control mechanism
must call to trigger a key press.

The VR control system provided here should be considered a temporary standin pulled from another
unpolished project. It's vaguely descended from an older version of Bastiaan Olij's godot-xr-tools library,
but heavily modified for non-standard gravity situations in a different project, which in turn makes it 
a bit cumbersome to re-adapt to normal situations. You have been warned.


TODO
----
- dynamically add key parent node? otherwise keys get lost if !editable-children
- need live adjustment of extra_gap. current impl has really weird fail with mouse jumping to viewport center
- _on_point* vr funcs should be on the collider, but pass through to parent VKR_Key.gd for easier setup
- Build a few more layouts
- Implement "dead key" character combos, or some suitable replacement
- Finish implementing more robust key definitions in the json
  - "gap" specifier? an offset for other keys in row?
- Make this an addon
- Make the text display easier to adapt to other things
- Be able to tie into Godot's normal UI system
- drumstick for key entry?
- word suggestion?


Author
-----
You can follow Tom's work here: https://twitter.com/TomsArtStuff
or here: https://tomlechner.com.
Please shower him with lots of money so he can keep working on things like this.


