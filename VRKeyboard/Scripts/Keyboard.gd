tool
extends Spatial


signal KeyPressed(key)
signal KeyReleased(key)


export(String, FILE) var keyboard_file

export(float, 0,90) var keyboard_row_tilt = 0 setget SetRowTilt #degrees to tilt vertically after each row
export(float, 0,120) var keyboard_key_tilt = 0 setget SetKeyTilt #degrees to tilt horizontally after each row
export(float, 0,90) var keyboard_row_a_offset = 0 setget SetRowAngleOffset #degrees to tilt horizontally after each row

func SetRowTilt(value):
	keyboard_row_tilt = value
	if keyboard_inited:
		ApplyTilt()
	else:
		print("Keyboard not inited, you must rebuild keyboard for tilt to be applied")
func SetKeyTilt(value):
	keyboard_key_tilt = value
	if keyboard_inited:
		ApplyTilt()
	else:
		print("Keyboard not inited, you must rebuild keyboard for tilt to be applied")
func SetRowAngleOffset(value):
	keyboard_row_a_offset = value
	if keyboard_inited:
		ApplyTilt()
	else:
		print("Keyboard not inited, you must rebuild keyboard for tilt to be applied")

var gap : float = 0
export(float) var extra_gap : float = 0 setget SetGap
func SetGap(value):
	extra_gap = value
	if keyboard_inited:
		LoadKeyboardFromJsonFile(keyboard_file, true)
	else:
		print("Keyboard not inited, you must rebuild keyboard for new gap to be applied")


var keyboard_name : String = "default"

#var layouts = [
#		"res://VRKeyboard/Layouts/en.json"
#	]


export(Array, float) var row_offsets : Array = [.05, 0.0, .05, 0.0, .05] #horizontal displacements

export(Array, PackedScene) var KeyPrefabs : Array

export(int, LAYERS_3D_PHYSICS) var key_collision_layer = 1  # collision layer to give to keys
export(int, LAYERS_3D_PHYSICS) var key_collision_mask = 1  # collision mask to give to keys


var KEY_EMOJI = 100
var KEY_ALTGR = 101
var KEY_DEAD1 = 102
var KEY_DEAD2 = 103
var KEY_DEAD3 = 104
var KEY_DEAD4 = 105

# Overrides of keys, specifies if key is a modifier, also which key prefab to use.
# Note this is not used if specials in provided in the layout json file.
var specials : Dictionary = {
		"Enter": { "model": 1, "cap": "Enter",  "value":      "\n"       },
		"Bksp":  { "model": 1, "cap": "Bksp",   "value":      "\b"       },
		"Tab":   { "model": 1, "cap": "Tab",    "value":      "\t"       },
		"Space": { "model": 2, "cap": "",       "value":      " "        },
		"Caps":  { "model": 1, "cap": "Caps",   "key":        "CAPSLOCK" },
		"Menu":  { "model": 1, "cap": "Menu",   "key":        "MENU"     },

		"Shift": { "model": 1, "cap": "Shift",  "mod_number": 1          },
		"Ctrl":  { "model": 1, "cap": "Ctrl",   "mod_number": 2          },
		"Win":   { "model": 1, "cap": "Win",    "mod_number": 3          },
		"Alt":   { "model": 1, "cap": "Alt",    "mod_number": 4          },
		
		"AltGr":  { "model": 1, "cap": "AltGr",  "mod_number": 5          },
		"Extra1": { "model": 1, "cap": "Alt",    "mod_number": 6          },
		"Extra2": { "model": 1, "cap": "Alt",    "mod_number": 7          },
		"Extra3": { "model": 1, "cap": "Alt",    "mod_number": 8          },
		"Extra4": { "model": 1, "cap": "Alt",    "mod_number": 9          },
		"Emoji":  { "model": 1, "cap": "Emoji",  "mod_number": 10         },
		
		"Up":       { "model": 1, "cap": "Menu",   "key":        "UP"     },
		"Down":     { "model": 1, "cap": "Menu",   "key":        "DOWN"    },
		"Left":     { "model": 1, "cap": "Menu",   "key":        "LEFT"    },
		"Right":    { "model": 1, "cap": "Menu",   "key":        "RIGHT"   },
		"PageUp":   { "model": 1, "cap": "Menu",   "key":        "PAGEUP"  },
		"PageDown": { "model": 1, "cap": "Menu",   "key":        "PAGEDOWN" },
		"Home":     { "model": 1, "cap": "Menu",   "key":        "HOME"     },
		"End":      { "model": 1, "cap": "Menu",   "key":        "END"     },
		
		"Ins":   { "model": 1, "cap": "Menu",   "key":        "INS"     },
		"Del":   { "model": 1, "cap": "Menu",   "key":        "DEL"     },
	}
var caps_on : int = 0


class KeyModel:
	var prefab : PackedScene
	var width : float
	var height : float

var key_prefabs : Array


class KeyInstance:
	var node


# compute key dimensions beforehand
func MapPrefabs():
	print ("map prefabs")
	key_prefabs = []
	for prefab in KeyPrefabs:
		var m = prefab.instance()
		var kk = KeyModel.new()
		kk.prefab = prefab
		var v = m.KeySize()
		kk.width = v.x
		kk.height = v.y
		key_prefabs.append(kk)


export(bool) var Build_Layout : bool = false setget DoBuildLayout
func DoBuildLayout(value):
	Build_Layout = false
	if value:
		BuildLayout()


# If keyboard_file isn't empty, then read from that.
# Else use a fallback layout defined above.
func BuildLayout():
	print ("Building keyboard...")
	
	if keyboard_file && keyboard_file != "":
		LoadKeyboardFromJsonFile(keyboard_file, false)
		return
	
	printerr("Missing json layout file, not building!")


export(bool) var Refresh_Layout : bool = false setget DoRefreshLayout
func DoRefreshLayout(value):
	Refresh_Layout = false
	if value:
		RefreshLayout()

# Set key viewport label.. sometimes they just get out of sync
func RefreshLayout():
	for child in $Keys.get_children():
		child.SetKeyCap(child.key_value, child.keyname)
		child.SetCollisionLayer(key_collision_layer, key_collision_mask)


func SaveKeyboardAsJson(to_file):
	print ("IMPLEMENT ME!!!")
	

var keyboard_json
var built_rows : Array

# File should be like "res://Keyboards/en.json"
func LoadKeyboardFromJsonFile(file, remap_only) -> bool:
	print ("Build from json file ", file, "...")
	
	if remap_only && !keyboard_inited:
		printerr("Keyboard must be built before remapping.")
		return false
	
	var data
	if remap_only:
		data = keyboard_json
	else:
		var data_file = File.new()
		if data_file.open(file, File.READ) != OK:
			printerr("Could not open keyboard file ", file, "!!")
			return false
			
		var data_text = data_file.get_as_text()
		data_file.close()
		var data_parse = JSON.parse(data_text)
		if data_parse.error != OK:
			printerr("Could not parse json from keyboard file ", file, "!!")
			return false
		data = data_parse.result
		keyboard_json = data
	
	
	# clear old keyboard
	var keys_parent = $Keys
	if !remap_only:
		while keys_parent.get_child_count() != 0:
			keys_parent.remove_child(keys_parent.get_child(0))
	
	if "name" in data:
		print ("Parsing keyboard ", data.name)
		keyboard_name = data.name
	
	if "gap" in data:
		gap = data.gap
	else: gap = 0
	
	# override key_prefabs
	if !remap_only:
		if "models" in data:
			key_prefabs = []
			for prefab in data.models:
				var m = load(prefab).instance()
				var kk = KeyModel.new()
				kk.prefab = prefab
				var v = m.KeySize()
				kk.width = v.x
				kk.height = v.y
				key_prefabs.append(kk)
		else:
			MapPrefabs()

	if "row_offsets" in data:
		row_offsets = data.row_offsets

	# for keys that use icons instead of text
	var key_icons = []
	if "icons" in data: # **** UNUSED!! IMPLEMENT ME!!
		for icon in data.icons:
			key_icons.append(load(icon))
		
	if "specials" in data:
		specials = data.specials
	
	
	# parse and lay out the rows
	var pos = Vector3.ZERO
	var r :int = 0
	var keynum : int = 0
	var cur_z = 0
	var row_height
	var row_heights = []
	var max_row_pos = 0
	var min_row_pos = 0
	var cur_row = 0
	
	#built_rows = []
	#var current_row
	
	var space_rx = RegEx.new()
	space_rx.compile("[^ ]+")
	
	for row in data.rows:
		row_height = 0
		pos.z = cur_z
		pos.x = 0
		if r < row_offsets.size(): pos.x += row_offsets[r]
		r += 1
		
		# rows can be either array of row parts
		# or a string of keys like "qQ wW eE"
		#
		# row parts can be either a key dict or a row string like "qQ wW eE"
		
		var row_parts
		if typeof(row) == TYPE_STRING:
			#row_parts = row.split(" ")
			#----
			row_parts = []
			for res in space_rx.search_all(row): # split on continuous spaces
				row_parts.append(res.get_string())
			
	
		else: row_parts = row # we have an array of keys
		
		for row_part in row_parts:
			if typeof(row_part) == TYPE_STRING:
				# shortcut like "qQ" or "qQ wW eE rR Tab"
				#var keys = row_part.split(" ")
				#----
				var keys = []
				for res in space_rx.search_all(row_part): # split on continuous spaces
					keys.append(res.get_string())
				var model : VRK_Key
				var special_info
				var prefab_i = 0
				
				for key in keys: # we have array of strings
					var offset = Vector3.ZERO
					
					if key in specials:
						special_info = specials[key]
						if 'model' in special_info:
							prefab_i = special_info['model']
							if prefab_i < 0 || prefab_i >= key_prefabs.size(): prefab_i = 0
						if 'offsety' in special_info:
							offset.z = special_info.offsety * key_prefabs[prefab_i].height
						if 'offsetx' in special_info:
							offset.x = special_info.offsetx * key_prefabs[prefab_i].width
					
					if remap_only:
						print ("keynum: ", keynum, ", numkids: ", keys_parent.get_child_count())
						model = keys_parent.get_child(keynum)
					else:
						model = key_prefabs[prefab_i].prefab.instance()
						keys_parent.add_child(model)
						model.owner = get_tree().edited_scene_root
					
					model.SetCollisionLayer(key_collision_layer, key_collision_mask)
					model.keyboard_row = cur_row
					model.keys = {}
					
					pos.x += key_prefabs[prefab_i].width/2
					 
					# adjust keyboard bounding box
					if keynum == 0:
						min_row_pos = pos.x - key_prefabs[prefab_i].width/2
						max_row_pos = min_row_pos
					else:
						var x = pos.x - key_prefabs[prefab_i].width/2
						if x < min_row_pos: min_row_pos = x
						x = pos.x + key_prefabs[prefab_i].width/2
						if x > max_row_pos: max_row_pos = x
						
					model.translation = pos + offset
					pos.x += key_prefabs[prefab_i].width/2 + gap+extra_gap + offset.x
					
					row_height = max(row_height, key_prefabs[prefab_i].height)
					
					keynum += 1
					
					if special_info:
						print ("special ", key)
						
						# should have one of: value key mod_number
						if !('value' in special_info): # so key or mod_number
							# text is same whether shift is on
							var cap = key
							if 'cap' in special_info: special_info['cap']
							
							model.SetKeyCap(key, key)
							if 'mod_number' in special_info:
								model.is_toggle = true
								model.mod_mask = (1 << int(special_info['mod_number']))
							else:
								model.special_key = special_info['key']
								model.is_toggle = false
							model.keys[0] = key
							
						else: # has value, so is really just a normal key with special treatment
							# text is same whether shift is on
							var cap = key
							if 'cap' in special_info: cap = special_info['cap']
							print ("key value: ", special_info['value'], " cap: ", cap, " special: ", special_info)
							
							model.SetKeyCap(special_info['value'], cap)
							model.keys[0] = special_info['value']
							
						model.name = "Key "+key
						if 'icon' in special_info:
							var icon = load(special_info['icon'])
							model.SetIcon(icon, special_info['icon'])
						
					else: # ordinary key where label equals the text sent
						var txt = key.substr(0,1)
						model.SetKeyCap(txt, txt)
						model.name = "Key "+txt
						
						for i in range(key.length()):
							model.keys[i] = key.substr(i,1)
							#model.keys[1] = key.substr(1,1)
					
			else: 
				# row_part not a string, expect key dict:
				#   caps: string, or array of { cap, icon, value, mods }
				#   model
				#   icon
				#   mod_number
				var key = row_part
				
				var modeli = 0
				var mod_number = 0
				var prefab_i = 0
				
				if 'model' in key:
					prefab_i = key['model']
					if prefab_i < 0 || prefab_i >= key_prefabs.size(): prefab_i = 0
				
				var model
				if remap_only:
					model = keys_parent.get_child(keynum)
				else:
					model = key_prefabs[prefab_i].prefab.instance()
					keys_parent.add_child(model)
					model.owner = get_tree().edited_scene_root
				model.keyboard_row = cur_row
				model.keys = {}
				model.SetCollisionLayer(key_collision_layer, key_collision_mask)
				
				pos.x += key_prefabs[prefab_i].width/2
				model.translation = pos
				
				row_height = max(row_height, key_prefabs[prefab_i].height)
				
				keynum += 1
				
				if "mod_number" in key:
					model.mod_mask = (1 << int(key['mod_number']))
					model.is_toggle = true
				
				
				if typeof(key.caps) == TYPE_STRING:
					# key has only a single label
					model.key_name = key.caps
					model.keys[0] = key.caps
				
				else: #expect caps array: {"cap": "Bksp", "icon": 0, "value": "\b", "mods":1 }
					for cap in key.caps:
						var icon = null
						var value = null
						var mods = 0
						var cap_str = ""
						
						if "icon" in cap:
							if cap.icon >= 0 && cap.icon < key_icons.size():
								icon = key_icons[cap.icon]
						if "cap" in cap:
							cap_str = cap.cap
						if "value" in cap:
							value = cap.value
						if "mods" in cap:
							# like "[1,3,6]" or "1"
							for mod in cap.mods:
								mods += (1 << mod)
							
						#make key with: cap.cap, value, icon
						model.keys[mods] = value
					model.SetKeyCap(model.keys[0], model.keys[0])
		
		if row_height == 0: row_height = gap+extra_gap
		row_heights.append(row_height)
		cur_z += row_height + gap+extra_gap
		cur_row += 1
	
	# center keyboard
	keyboard_inited = true
	keyboard_width = max_row_pos - min_row_pos
	keyboard_height = row_heights[0]
	for i in range(1, row_heights.size()):
		keyboard_height += gap+extra_gap + row_heights[i]
		
	for key in $Keys.get_children():
		key.translation = key.translation - Vector3(min_row_pos + keyboard_width/2,0,0)
		key.keyboard_pos = Vector2(key.translation.x, key.translation.z)
	
	ApplyTilt()
	return true


var keyboard_width
var keyboard_height
var keyboard_inited : bool = false

# Remap keyboard to a torus. Not that this requires state from a recent build, so you need to rebuild keyboard before this works.
func ApplyTilt():
	#print ("kb width: ", keyboard_width, ", kb height: ", keyboard_height)
	
	if !keyboard_inited:
		printerr("Keyboard needs to be newly built for ApplyTilt to work.")
		return
	
	var row_tilt = keyboard_row_tilt
	if row_tilt == 0: row_tilt = 1e-3
	
	var key_tilt = keyboard_key_tilt
	if key_tilt == 0: key_tilt = 1e-3
	
	var r = 0 #torus ring center
	#var board_offset = deg2rad(keyboard_row_a_offset)
	if key_tilt > 0: r = keyboard_width/2 / tan(deg2rad(key_tilt/2))
	
	var r2 = 0 #torus ring radius
	if row_tilt > 0: r2 = keyboard_height/2 / tan(deg2rad(row_tilt/2))
	
	var center = Vector3.ZERO
	for key in $Keys.get_children():
		#print (key.name, "  ", (key.keyboard_pos.y + keyboard_height/2) / keyboard_height)
		
		var theta = 0
		var theta_x = 0
		var theta_key = 0
		var rr = 0
		var pos = Vector3(key.keyboard_pos.x, 0, key.keyboard_pos.y)
		
		if row_tilt != 0:
			theta_x = deg2rad(90+keyboard_row_a_offset - row_tilt/2) +  (key.keyboard_pos.y + keyboard_height/2) / keyboard_height * deg2rad(row_tilt)
			var tpos = r2*Vector2(sin(theta_x), cos(theta_x))
			pos = Vector3(key.keyboard_pos.x, tpos.y, tpos.x)
			rr = r + tpos.x # radius of key
			
			#var theta = pos.x / (keyboard_width/2) * deg2rad(keyboard_key_tilt)/2
			theta = pos.x / rr
			theta_key = -rad2deg(theta)
		
		if abs(key_tilt) > 1e-5:
			#var rrr = rr + keyboard_height - pos.z
			#theta *= r/rr
			pos = Vector3(rr * sin(theta), pos.y, rr*(1-cos(theta)) + (r-rr))
			
		key.rotation_degrees = Vector3(180-rad2deg(theta_x), theta_key, 0)
		key.translation = pos
		
		center += pos
		
	# make keyboard centered on origin, instead of far out as with high radius torii
	center /= $Keys.get_child_count()
	
	for key in $Keys.get_children():
		key.translation -= center



#------------------------------ Runtime funcs ----------------------------------------

func _ready():
	MapPrefabs()
	
	if !Engine.editor_hint:
		if $Keys.get_child_count() == 0: BuildLayout()
		else: 
			#call_deferred("RefreshLayout")
			RefreshLayout() # *** THIS IS A HACK!! WHY IS THIS NECESSARY??!!?!? (Label within viewport does not preserve state)

	for child in $Keys.get_children():
		child.connect("KeyPressed", self, "KeyPressed")


var cur_mods = 0

func KeyPressed(key):
	print ("keyboard, pressed: ", key.key_value)
	if key.is_toggle:
		if key.is_pressed:
			cur_mods |= key.mod_mask
			if typeof(key.special_key) == TYPE_STRING && key.special_key == "CAPSLOCK": caps_on = true
		else:
			cur_mods = (cur_mods & ~key.mod_mask)
			if typeof(key.special_key) == TYPE_STRING && key.special_key == "CAPSLOCK": caps_on = false
		print ("set mods: ", cur_mods)
		for child in $Keys.get_children():
			child.UpdateCaps(cur_mods)
	else:
		emit_signal("KeyPressed", key)
		if cur_mods != 0 && !caps_on:
			# absorb the mods
			cur_mods = 0
			for child in $Keys.get_children():
				if child.is_toggle and child.is_pressed:
					child.OnClick()
				else: child.UpdateCaps(cur_mods)


#func ModifiersChanged():
#	# step through each key and update keycap
#	pass


# TODO !!!! ...if necessary... probably easier for users to just display different key caps
func JoinCharacters(ch1, ch2) -> String:
	match ch1:
		"'":
			match ch2:
				'e': return '\u0233'
				'a': return '\u0301'
				'o': return '\u0243'
		':':
			match ch2:
				'i': return '\u0239'
		'~':
			match ch2:
				'n': return '\u0241'
		
	return ch1+ch2




