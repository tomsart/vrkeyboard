tool
extends Spatial
#extends RigidBody
class_name VRK_Key

signal KeyPressed(key)
signal KeyReleased(key)


 # string that key CURRENTLY sends on click.. will be changed by Keyboard on shift/alt/etc presses
export(String) var key_value : String = ""
export(String) var keyname : String # what is CURRENTLY displayed on the key
export(String) var icon_resource : String

export(int) var special_key : int = 0
export(int) var mod_mask : int = 0
export(bool) var is_toggle : bool = false
export(Vector2) var Key_Size : Vector2 # if 0, return $KeyRound size

# values are possible text to send, keys are corresponding modifier mask. 0 MUST ALWAYS BE for no mods
export(Dictionary) var keys : Dictionary # = {mod_mask: "cap"}

#var keyboard_pos : Vector2 # place on untilted keyboard so we can tilt more easily

var is_pressed : bool = false # whether button is currently considered pressed, regardless of current state of animation
var playdir = 0 # non moving: 0, being pressed down: 1, returning to not pressed: -1

var using_icon : bool = false
var keyboard_pos : Vector2
var keyboard_row : int


func _ready():
	if icon_resource != "": using_icon = true
	print (name, " USING RESOURCE: ", using_icon)


#----------------- Generic response funcs -----------------------------------
#------- usually, subclasses should only need to change these ---------------

var original_color # for hovering

func StartHover():
	print ("start hover ", name)
	var mat = $KeyParent/KeyRound.get_surface_material(1)
	if !mat: return
	original_color = mat.albedo_color
	mat.albedo_color = Color(1.0, 1.0, 1.0)

func EndHover():
	print ("end hover ", name)
	var mat = $KeyParent/KeyRound.get_surface_material(1)
	if !mat: return
	mat.albedo_color = original_color


# This is called each time a "click" happens for any reason.
func OnClick(): # update animation state
	if is_toggle:
		if is_pressed:
			is_pressed = false
			$KeyParent/AnimationPlayer.play_backwards("Push")
			playdir = -1
		else:
			is_pressed = true
			$KeyParent/AnimationPlayer.play("Push")
			playdir = 1
		return
	
	if playdir == 0: #start playing
		$KeyParent/AnimationPlayer.play("Push")
		playdir = 1;
	elif playdir == -1: #was playing backwards, start playing forwards
		playdir = 1
		$KeyParent/AnimationPlayer.play("Push")

# This is called each time a "click" is released for any reason. Ignored by default.
func OnClickOff():
	pass


# Animation state upkeep. Currently just reversed "Push" for non-toggle buttons.
func _on_AnimationPlayer_animation_finished(anim_name):
	if is_toggle:
		playdir = 0
		return
	
	if playdir == 1:
		$KeyParent/AnimationPlayer.play_backwards("Push")
		playdir = -1
	elif playdir == -1:
		playdir = 0


#---------------------- Keyboard setup helper funcs -------------------------------

# Width,Height to use for placement information. Assume centered on origin.
func KeySize() -> Vector2:
	if (Key_Size.length() < .0001):
		#print ("transform aabb: ",$KeyParent/KeyRound.get_transformed_aabb().size)
		#print ("aabb: ",$KeyParent/KeyRound.get_aabb().size)
		
		var sz = $KeyParent/KeyRound.get_transformed_aabb()
		#print ("rotation.y = ", rotation.y)
		if $KeyParent/KeyRound.rotation.y != 0: return Vector2(sz.size.z, sz.size.x) # ** HACK!!! transformed aabb is not getting transformed at time of query
		return Vector2(sz.size.x, sz.size.z)
	return Key_Size


func SetCollisionLayer(layer, mask):
	$KeyParent.collision_layer = layer
	$KeyParent.collision_mask = mask


# kname is the key label that people see, and text is the text that is sent when clicked
func SetKeyCap(text, kname):
	key_value = text
	if kname and kname != "": keyname = kname
	else: keyname = text
	
	if using_icon:
		print ("USING ICON ", name, " ", icon_resource)
		if $KeyParent/Keyface/Viewport/KeyCap/Sprite.texture != null: return
		$KeyParent/Keyface/Viewport/KeyCap/Sprite.texture = load(icon_resource)
		$KeyParent/Keyface/Viewport.render_target_update_mode = Viewport.UPDATE_ONCE
		$KeyParent/Keyface/Viewport/KeyCap/Label.text = ""
	else:
		$KeyParent/Keyface/Viewport/KeyCap/Label.text = keyname
	
	$KeyParent/Keyface/Viewport.render_target_update_mode = Viewport.UPDATE_ONCE

# Use an icon instead of text
func SetIcon(icon, iconstr):
	$KeyParent/Keyface/Viewport/KeyCap/Sprite.texture = icon
	$KeyParent/Keyface/Viewport/KeyCap/Label.text = ""
	using_icon = true
	self.icon_resource = iconstr

# Set the key label and text according to cur_mods
func UpdateCaps(cur_mods):
	if using_icon:
		return
	
	if cur_mods in keys:
		key_value = keys[cur_mods]
	else:
		key_value = keys[0]
	if special_key == 0 && keys.size() > 1:
		$KeyParent/Keyface/Viewport/KeyCap/Label.text = key_value
		$KeyParent/Keyface/Viewport.render_target_update_mode = Viewport.UPDATE_ONCE


#------------------------------ VR Events --------------------------

# VR point-and-click callback
func _on_pointer_pressed(collided_at):
	print (key_value, " pressed in vr!")
	OnClick()
	emit_signal("KeyPressed", self)

# VR point-and-click released callback
func _on_pointer_released(collided_at):
	print (key_value, " released in vr!")
	OnClickOff()
	emit_signal("KeyReleased", self)

# VR hover on callback
func _on_pointer_hover_start():
	print (key_value, " hovered in vr!")
	StartHover()

# VR hover off callback
func _on_pointer_hover_end():
	print (key_value, " end hovered in vr!")
	EndHover()


#--------------------------- Non-vr Events -------------------------------

# Normal mouse click signal
func _on_Key_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == 1:
			if event.pressed:
				#print (keycap, " pressed!")
				OnClick()
				emit_signal("KeyPressed", self)
			else:
				#print (keycap, " released!")
				emit_signal("KeyReleased", self)

func _on_KeyParent_mouse_entered():
	StartHover()

func _on_KeyParent_mouse_exited():
	EndHover()


#--------------------------- Collision Events -------------------------------

func _on_Key_body_entered(body):
	print ("Body ", body.name," entered Key ", name, ", parent: ", body.get_parent().name)
	OnClick()
	emit_signal("KeyPressed", self)


func _on_Key_area_entered(area):
#	print ("Area ", area.name," entered Key ", name,
#	 ", key layer their mask: ", $KeyParent.collision_layer & area.collision_mask,
#	 ", key mask their layer: ", $KeyParent.collision_mask & area.collision_layer
#	)
	if $KeyParent.collision_layer & area.collision_layer == 0: return
	
	OnClick()
	emit_signal("KeyPressed", self)


