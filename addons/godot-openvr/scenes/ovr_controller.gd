extends ARVRController

signal controller_activated(controller)

#export var show_controller_mesh = true setget set_show_controller_mesh, get_show_controller_mesh
export (NodePath) var custom_mesh

var custom_mesh_node : Spatial

#func set_show_controller_mesh(p_show):
#    show_controller_mesh = p_show
#    if controller_mesh:
#        controller_mesh.visible = p_show
#
#func get_show_controller_mesh():
#    return show_controller_mesh

var ovr_render_model
var controller_mesh
var components = Array()
var ws = 0

func _ready():
	# instance our render model object
	
	# set our starting vaule
	if !custom_mesh.is_empty():
		custom_mesh_node = get_node(custom_mesh)
		controller_mesh = custom_mesh_node
	else: #fallback mesh, gets filled with ovr_render_model
		ovr_render_model = preload("res://addons/godot-openvr/OpenVRRenderModel.gdns").new()
		controller_mesh = $Controller_mesh
	#controller_mesh.visible = show_controller_mesh
	
	# hide to begin with
	visible = false

func apply_world_scale():
	var new_ws = ARVRServer.world_scale
	if (ws != new_ws):
		ws = new_ws
		controller_mesh.scale = Vector3(ws, ws, ws)

func load_controller_mesh(controller_name):
	if ovr_render_model.load_model(controller_name.substr(0, controller_name.length()-2)):
		return ovr_render_model
	
	if ovr_render_model.load_model("generic_controller"):
		return ovr_render_model
	
	return Mesh.new()

func _process(delta):
	if !get_is_active():
		if visible:
			visible = false
			var controller_name = get_controller_name()
			print("Controller " + controller_name + " became inactive")
		return
	
	# always set our world scale, user may end up changing this
	apply_world_scale()
	
	if visible:
		return
	
	# became active? make sure our controller is visible
	var controller_name = get_controller_name()
	print("Controller " + controller_name + " became active")

	# attempt to load a mesh for this
	if "mesh" in controller_mesh and controller_mesh.mesh == null:
		# hope this is a MeshInstance....
		controller_mesh.mesh = load_controller_mesh(controller_name)

	# make it visible
	visible = true
	emit_signal("controller_activated", self)
