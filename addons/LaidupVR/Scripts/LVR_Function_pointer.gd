extends Spatial

export (int) var click_button = 15
export (int) var parent_depth_check = 2 # check for _on_pointer_* up this many nodes if func not found
#export (int, FLAGS, "1", "2", "3") var collision_mask 

var target = null
var last_collided_at = Vector3(0, 0, 0)
var laser_y = -0.05
onready var ws = ARVRServer.world_scale


func set_enabled(p_enabled):
	$Laser.visible = p_enabled
	$RayCast.enabled = p_enabled

func set_collision_mask(mask):
	$RayCast.collision_mask = mask

func FindParentWithMethod(target, method, depth):
	while depth >= 0:
		if target.has_method(method): return target
		target = target.get_parent()
		depth = depth - 1
	return null

func _on_button_pressed(p_button):
	if p_button == click_button and $RayCast.enabled:
		print ("trying to click from pointer")
		if $RayCast.is_colliding():
			target = $RayCast.get_collider()
			last_collided_at = $RayCast.get_collision_point()
			print("Button pressed on " + target.get_name() + " at " + str(last_collided_at))
			var targ = FindParentWithMethod(target, "_on_pointer_pressed", parent_depth_check)
			if targ:
				targ._on_pointer_pressed(last_collided_at)
		else: print ("pointer not colliding")

func _on_button_release(p_button):
	if p_button == click_button and target:
		# let object know button was released
		# print("Button released on " + target.get_name())
		#target = FindParentWithMethod(target, "_on_pointer_release", parent_depth_check)
		
		# we hope we don't have to search for parent target:
		if target and target.has_method("_on_pointer_release"):
			target._on_pointer_release(last_collided_at)

		target = false
		last_collided_at = Vector3(0, 0, 0)

func _ready():
	# Get button press feedback from our parent (should be an ARVRController)
	get_parent().connect("button_pressed", self, "_on_button_pressed")
	get_parent().connect("button_release", self, "_on_button_release")
	
	# apply our world scale to our laser position
	$Laser.translation.y = laser_y * ws

func _process(delta):
	var new_ws = ARVRServer.world_scale
	if (ws != new_ws):
		ws = new_ws
	$Laser.translation.y = laser_y * ws
	
	#print ("ray enabled: ", $RayCast.enabled, " colliding: ", $RayCast.is_colliding())
	if $RayCast.enabled and $RayCast.is_colliding():
		var new_at = $RayCast.get_collision_point()
		var new_target = $RayCast.get_collider()
		#print ("pointer target: ", new_target.name, ", at: ", new_at)
		
		if new_at == last_collided_at:
			pass
		elif target:
			# if target is set our mouse must be down, we keep sending events to our target
			if target.has_method("_on_pointer_moved"):
				target._on_pointer_moved(new_at, last_collided_at)
		else:
			if new_target.has_method("_on_pointer_moved"):
				new_target._on_pointer_moved(new_at, last_collided_at)
		
		last_collided_at = new_at

