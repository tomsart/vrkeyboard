extends Node

# This is supposed to be a hub to provide reactions when various buttons 
# and track pads are pressed... need to implement signals for things


# Axis 0 = Left/Right on the touchpad
# Axis 1 = Forward/Backward on the touchpad
# Axis 2 = Front trigger
# Button 1 = menu button
# Button 2 = side buttons
# Button 14 = press down on touchpad
# Button 15 = Front trigger (on/off version of Axis 2)

export (NodePath) var Controller 

var controller : ARVRController
onready var trackIndicator : Spatial = get_parent().get_node("TrackIndicator")

var radius = .1

# Called when the node enters the scene tree for the first time.
func _ready():
	if !Controller.is_empty(): controller = get_node(Controller)
	else: 
		controller = get_parent().get_parent()
		if controller.name != "Left_Hand" and controller.name != "Right_Hand":
			controller = get_parent()
		
	set_process(false)
	print ("Button feedback found controller: ", controller.name)
	controller.connect("controller_activated", self, "ControllerActivated")

func ControllerActivated(cont):
	print ("tracker controller activated! ", cont.name)
	set_process(true)

func _process(delta):
	var x = controller.get_joystick_axis(0)
	var y = controller.get_joystick_axis(1)
	var t = controller.get_joystick_axis(2)
	#print ("tracker process! ", x," ",y," ",t)
	
	trackIndicator.translation.x = x * radius
	trackIndicator.translation.z = -y * radius
	
	#print ("axes: ", x, "  ", y, "  ", t)

