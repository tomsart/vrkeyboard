extends ARVRController

signal controller_activated(controller)


var ws = 0
var controller_is_loaded = false

#func apply_world_scale():
#	var new_ws = ARVRServer.world_scale
#	if (ws != new_ws):
#		ws = new_ws
#		scale = Vector3(ws, ws, ws)


export (bool) var doControllerHack : bool = false setget DoControllerHack
var otherController : ARVRController

export var show_controller_mesh = true setget set_show_controller_mesh, get_show_controller_mesh

func set_show_controller_mesh(p_show):
	show_controller_mesh = p_show
	if $OVRRenderModel:
		$OVRRenderModel.visible = p_show

func get_show_controller_mesh():
	return show_controller_mesh

func _ready():
	# set our starting vaule
	$OVRRenderModel.visible = show_controller_mesh
	
	if doControllerHack:
		DoControllerHack(true)
	
	# hide to begin with
	visible = false


##  This hack is for strange error in recent (april 2020) godot openvr
##  where you press a button on one controller, and a button down event is
##  sent seemingly from the other controller... something to do with the new
##  action system which I don't quite understand

func DoControllerHack(value):
	doControllerHack = value
	if doControllerHack:
		if name == "Left_Hand":
			otherController = get_node("../Right_Hand")
		else: 
			otherController = get_node("../Left_Hand")
	else: otherController = null

func ControllerHACK():
	if doControllerHack && otherController: return otherController
	else: return self


func _process(delta):
	if !get_is_active():
		visible = false
		return
	
	#apply_world_scale()
	
	if visible:
		return
	
	# make it visible
	visible = true
	emit_signal("controller_activated", self)
