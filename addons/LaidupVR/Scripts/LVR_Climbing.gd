extends Spatial
class_name LVR_Climbing

# Note, vive buttons:
# Axis 0 = Left/Right on the touchpad
# Axis 1 = Forward/Backward on the touchpad
# Axis 2 = Front trigger
# Button 1 = menu button
# Button 2 = side buttons
# Button 14 = press down on touchpad
# Button 15 = Front trigger (on/off version of Axis 2)

export(int) var climb_button : int = 2
export(float) var grab_threshhold : float = .3
export (int, LAYERS_3D_PHYSICS) var raycast_mask = 1

export(NodePath) var LeftController
export(NodePath) var RightController
export(NodePath) var TheCamera

var left_controller : Spatial
var right_controller : Spatial
var camera : Spatial
var origin_node : Spatial
var left_raycast : RayCast
var right_raycast : RayCast
export (float) var FloorSlope : float = 45 # degrees
var floor_slope_limit : float # cos of FloorSlope

var previous_l : Transform
var previous_r : Transform
var previous_c : Transform

var need_init : bool = true
var right_down = false
var left_down = false

export(float) var lerp_to_stand_dur = .25
var lerp_to_stand = false
var lerp_to_stand_time = 0
var lerp_to_stand_pos : Vector3
var lerp_to_stand_start : Vector3

var transform_reset : Transform

func IsClimbing():
	return right_down || left_down

# Called when the node enters the scene tree for the first time.
func _ready():
	var pteranodon = get_node("../../Pteranodon")
	pteranodon.get_node("AnimationPlayer2").play("FlyInto")
	pteranodon.get_node("AnimationPlayer2").seek(0, true)
	pteranodon.get_node("AnimationPlayer2").stop()
	
	floor_slope_limit = cos(FloorSlope);
	
	if !LeftController.is_empty():
		left_controller = get_node(LeftController)
	else:
		left_controller = get_node("../Left_Hand")
	if !RightController.is_empty():
		right_controller = get_node(RightController)
	else:
		right_controller = get_node("../Right_Hand")
	if !TheCamera.is_empty():
		camera = get_node(TheCamera)
	else:
		camera = get_node("../ARVRCamera")
	
	origin_node = get_parent()
	transform_reset = origin_node.transform
	
#	left_raycast = left_controller.get_node("FunctionPickup2/RayCast")
#	right_raycast = right_controller.get_node("FunctionPickup2/RayCast")
	
	var rayrot = Vector3(-27,45,-19)
	
	left_raycast = RayCast.new()
	left_raycast.name = "RayLeftClimb"
	left_raycast.rotation_degrees = Vector3(rayrot.x, -rayrot.y, rayrot.z)
	left_raycast.cast_to = Vector3(0,0,-grab_threshhold)
	left_raycast.collision_mask = raycast_mask
	left_raycast.enabled = true
	left_controller.add_child(left_raycast)
	
	right_raycast = RayCast.new()
	left_raycast.name = "RayRightClimb"
	right_raycast.rotation_degrees = rayrot
	right_raycast.cast_to = Vector3(0,0,-grab_threshhold)
	right_raycast.collision_mask = raycast_mask
	right_raycast.enabled = true
	right_controller.add_child(right_raycast)
	
	previous_l = left_controller.transform
	previous_r = right_controller.transform
	previous_c = camera.transform
	

func _physics_process(delta):
	
#	if left_raycast.is_colliding():
#		$HitL/CubeMesh.global_transform.origin = left_raycast.get_collision_point()
#		$HitL/CubeMesh.visible = true
#	else: $HitL/CubeMesh.visible = false
#	if right_raycast.is_colliding():
#		$HitR/CubeMesh.global_transform.origin = right_raycast.get_collision_point()
#		$HitR/CubeMesh.visible = true
#	else: $HitR/CubeMesh.visible = false
	
	if lerp_to_stand:
		print ("lerping to stand ", lerp_to_stand_time)
		lerp_to_stand_time += delta / lerp_to_stand_dur
		if lerp_to_stand_time >= 1:
			lerp_to_stand = false
			lerp_to_stand_time = 1
		var t = ease(lerp_to_stand_time, 3.4)
		var pos = lerp(lerp_to_stand_start, lerp_to_stand_pos, t)
		origin_node.transform.origin = pos
	
	if !IsClimbing():
		#print ("not climbing")
		return
		
	if need_init: # guard against ugly initial hitches
		previous_l = left_controller.transform
		previous_r = right_controller.transform
		previous_c = camera.transform
		need_init = false
		print ("climbing: Init'd prev transforms")
		return
	
	var mid : Vector3 = Vector3.ZERO
	var avg = 0
	if left_down:
		mid += left_controller.transform.origin
		avg += 1
	if right_down:
		mid += right_controller.transform.origin
		avg += 1
	mid /= avg
	
	var mid_prev : Vector3 = Vector3.ZERO
	avg = 0
	if left_down:
		mid_prev += previous_l.origin
		avg += 1
	if right_down:
		mid_prev += previous_r.origin
		avg += 1
	mid_prev /= avg
	
	
	var d_cur  = origin_node.transform.xform(mid) - origin_node.transform.xform(camera.transform.origin)
	var d_prev = origin_node.transform.xform(mid_prev) - origin_node.transform.xform(previous_c.origin)
	
	var dp = d_prev - d_cur
	
	print ("climb vector: ", dp)
	
	origin_node.transform.origin += dp #... probably not right when base is scaled
	
	previous_l = left_controller.transform
	previous_r = right_controller.transform
	previous_c = camera.transform

# Might need to stand up if head is over a flat enough surface
func CheckStandup(controller : Spatial, raycast : RayCast):
	if !raycast.is_colliding():
		lerp_to_stand = false
		return;
	
	var hitpoint : Vector3 = raycast.get_collision_point()
	var hitnormal : Vector3 = raycast.get_collision_normal()
	
	var down : Vector3 = -origin_node.global_transform.basis.y
	var head_over_hand = camera.global_transform.origin - controller.global_transform.origin
	down = down.normalized()
	head_over_hand = head_over_hand.normalized()
	var ddot = down.dot(head_over_hand)
	print ("Maybe stand, downdot: ", ddot)
	if (ddot < -.9): # is head mostly over hands?
		# find flat point straight down
		var space_state = get_world().direct_space_state
		var hit = space_state.intersect_ray(camera.global_transform.origin,
					origin_node.global_transform.xform(camera.transform.origin
						 - Vector3(0,1.1 * camera.transform.origin.y,0)),
						[], raycast_mask, true, true)
		if !hit.empty():
			print ("dist to climp point: ", (hit.position - hitpoint).length())
			hitpoint = hit.position
			hitnormal = hit.normal
			
			if down.dot(hitnormal) < -floor_slope_limit:
				print ("lerp to stand....")
				lerp_to_stand = true
				lerp_to_stand_start = origin_node.transform.origin
				lerp_to_stand_time = 0
				

				var diff = camera.transform.origin
				diff.y = 0
				diff = origin_node.transform.basis.xform(diff)
				hitpoint = origin_node.get_parent().global_transform.xform_inv(hitpoint)
				lerp_to_stand_pos = hitpoint - diff
				# var height = abs(camera.transform.origin.y)

# make sure hand animations and positions are appropriate for climbing
func UpdateClimbGrab():
	pass

#------------------------------------------------------------------
#------------------------- Controller signals ---------------------
#------------------------------------------------------------------

func _on_Left_Hand_button_pressed(button):
	print ("MAYBE Left grab for climbing on ", button)
	
	if button == 1: # menu
		var pteranodon = get_node("../../Pteranodon")
		pteranodon.visible = true
		var player = pteranodon.get_node("AnimationPlayer2")
		player.play("FlyInto")
		return
	
	if button != climb_button:
		print ("...wrong button no climb")
		return
	if !left_raycast.is_colliding(): 
		print ("...raycast not colliding, no climb")
		return
	var d = (left_raycast.get_collision_point() - left_raycast.global_transform.origin).length()
	print ("dist: ", d)
	if  d > grab_threshhold:
		print ("...to farr, no climb")
		return
	
	print ("Left grab for climbing on ")
	if !IsClimbing(): need_init = true
	left_down = true
	lerp_to_stand = false

func _on_Left_Hand_button_release(button):
	if button != climb_button: return
	if !left_down: return
	
	# *** release hand grab pose
	
	left_down = false
	if !IsClimbing():
		print ("stopped climbing")
		CheckStandup(left_controller, left_raycast)

func PushOff():
	pass

func _on_Right_Hand_button_pressed(button):
	print ("MAYBE Right grab for climbing on ", button)
	
	if button == 1: # menu
		# manually push off, or reset
		var direct_move = get_node("../Left_Hand/LVR_Direct_move")
		if direct_move.gravity == 0: #push
			print ("AAAAAAA!!!!")
			direct_move.gravity = -20
			direct_move.use_drag = false
			direct_move.set_velocity(Vector3(0,0,-10))
		else:
			print ("Reset")
			direct_move.gravity = 0
			direct_move.use_drag = true
			direct_move.set_velocity(Vector3(0,0,0))
			origin_node.transform = transform_reset
		return
		
	if button != climb_button:
		print ("...wrong button no climb")
		return
	if !right_raycast.is_colliding(): 
		print ("...raycast not colliding, no climb")
		return
	
	var d = (right_raycast.get_collision_point() - right_raycast.global_transform.origin).length()
	print ("dist: ", d)
	if d > grab_threshhold:
		print ("...to farr, no climb")
		return
	
	print ("Right grab for climbing on ")
	if !IsClimbing(): need_init = true
	right_down = true
	lerp_to_stand = false

func _on_Right_Hand_button_release(button):
	if button != climb_button: return
	if !right_down: return
	
	# *** release hand grab pose
	
	right_down = false
	if !IsClimbing():
		print ("stopped climbing")
		CheckStandup(right_controller, right_raycast)
		
		

# pteranodon hits headlight
func _on_Cube_area_entered(area):
	if area.name == "Headlight":
		print ("pteranodon hit! ", area.name)
		print ("AAAAAAA!!!!")
		var direct_move = get_node("../Left_Hand/LVR_Direct_move")
		direct_move.gravity = -20
		direct_move.use_drag = false
		direct_move.set_velocity(Vector3(0,0,-10))
		
	
