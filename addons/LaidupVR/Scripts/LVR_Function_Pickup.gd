extends Area
class_name LVR_Function_Pickup

# Object pickup/drop/pull with extra pointer mechanisms.
# Pickupable objects need to have a "pick_up" method

# Note, vive buttons:
# Axis 0 = Left/Right on the touchpad
# Axis 1 = Forward/Backward on the touchpad
# Axis 2 = Front trigger
# Button 1 = menu button
# Button 2 = side buttons
# Button 14 = press down on touchpad
# Button 15 = Front trigger (on/off version of Axis 2)

signal has_picked_up(what)
signal has_dropped

signal started_hovering(obj)
signal stopped_hovering(obj)
signal clicked_on_hovered(obj)

signal grab_on(obj)
signal grab_off(obj)


export var impulse_factor = 2.0
export var pickup_button_id = 2 # for picking up, pulling, and reparenting behavior
export var action_button_id = 15 # For _on_pointer_pressed calls
export (float) var pull_power = .35
export (float) var pull_power2 = 0
export (bool) var hover_with_laser = false

export (int) var v_sample_size = 10 # how many previous velocities to average over
var velocity_samples : Array = []
var v_sample_current : int = 0

# Prefered position to hold objects. 
# TODO: exact spot changes with size, and in -y direction of holdat
# TODO: different ways to hold things, like grasping, pinching, balancing, etc
export (NodePath) var HoldAt

var picked_up_object = null # object currently being held

# Tracking of objects that have entered immediate grab zone
var object_in_area = Array()
var closest_object = null

# pointer functionality
export (bool) var scan_for_hover : bool = true
var hovered_object : Spatial = null
var hovered_point : Vector3

# tracking velocity for smooth connection and release
var last_position : Vector3 = Vector3.ZERO # of self, for throwing objects
var global_velocity : Vector3 = Vector3.ZERO # of self
#for debugging:
#var last_parent_pos : Vector3 = Vector3.ZERO
#var velocity_in_parent : Vector3 = Vector3.ZERO #of self in parent ref frame, for throwing objects

# immediate grab lerping
var doing_lerp : bool = false
var lerp_time : float = 0
var lerp_max_time : float = 1
var lerp_target : Vector3 = Vector3.ZERO
var grab_velocity : Vector3 # velocity in controller reference frame

# using forces to pull things closer
var doing_pull_search : bool = false
var doing_pull : bool = false
var pull_object : RigidBody
var pull_object_at : Vector3

func _ready():
	if !HoldAt.is_empty(): 
		print ("hold at: ", HoldAt)
		lerp_target = global_transform.xform_inv(get_node(HoldAt).global_transform.origin)
		
	# get button events from controller
	get_parent().connect("button_pressed", self, "_on_button_pressed")
	get_parent().connect("button_release", self, "_on_button_release")
	
	last_position = global_transform.origin
	#last_parent_pos = get_parent().translation
	
	for i in range(v_sample_size): velocity_samples.append(0)


func _on_Function_Pickup_body_entered(body):
	print (body.name, " body entered pickup on ", get_parent().name)
	
	# if we were pulling object that suddenly enters our immediate grab zone,
	# then convert to immediate grab
	if (doing_pull or doing_pull_search) and body == pull_object:
		if doing_pull:
			doing_pull = false
			if pull_object.has_method("DecreasePullCount"):
				print ("Increase pull count on ", pull_object.name)
				pull_object.DecreasePullCount()
			pull_object = null
		if doing_pull_search:
			doing_pull_search = false
			TurnOffPullLaser()
		_pick_up_object(body)
		
	# add our object to our array if required
	if body.has_method('pick_up') and object_in_area.find(body) == -1:
		object_in_area.push_back(body)
		_update_closest_object()

func _on_Function_Pickup_body_exited(body):
	print ("body left pickup on ", get_parent().name)
	# remove our object from our array
	if object_in_area.find(body) != -1:
		object_in_area.erase(body)
		_update_closest_object()

# This sorts objects that have entered the grab sphere
func _update_closest_object():
	var new_closest_obj = null
	if !picked_up_object:
		var new_closest_distance = 1000
		for o in object_in_area:
			# only check objects that aren't already picked up
			if o.is_picked_up() == false:
				var delta_pos = o.global_transform.origin - global_transform.origin
				var distance = delta_pos.length()
				if distance < new_closest_distance:
					new_closest_obj = o
					new_closest_distance = distance
	
	if closest_object != new_closest_obj:
		# remove highlight on old object
		if closest_object:
			closest_object.decrease_is_closest()
		
		# add highlight to new object
		closest_object = new_closest_obj
		if closest_object:
			closest_object.increase_is_closest()

func drop_object():
	if picked_up_object:
		# let go of this object
		#for debugging:
		#print ("Drop object, global v: ", global_velocity, "  parent v: ", velocity_in_parent)
		print ("Drop object, global v: ", global_velocity)
		
		#let_go() should set rigid body if necessary
		#picked_up_object.let_go(global_velocity * impulse_factor)
		picked_up_object.let_go(AverageVelocities() * impulse_factor)
		
		#picked_up_object.global_transform.origin = get_node(HoldAt).global_transform.origin
		picked_up_object.global_transform.origin += picked_up_object.linear_velocity \
						/ ProjectSettings.get_setting("physics/common/physics_fps")
		picked_up_object = null
		emit_signal("has_dropped")
		GrabOff()

# Like _pick_up_object, but call at any time for any object.
# Initiates lerp to HoldAt.
func PickupImmediately(p_object):
	if picked_up_object == p_object:
		return
	
	# holding something else? drop it
	if picked_up_object:
		drop_object()
	
	if p_object:
		grab_velocity = global_transform.basis.xform_inv(p_object.linear_velocity)
		doing_lerp = false
		lerp_time = 0
		picked_up_object = p_object
		picked_up_object.pick_up(self, get_parent()) # reparent is done here
		picked_up_object.translation = global_transform.xform_inv(get_node(HoldAt).global_transform.origin)
		emit_signal("has_picked_up", picked_up_object)
		GrabOn()

# Claim this object as being held. Initiate lerp to final holding position.
func _pick_up_object(p_object):
	# already holding this object, nothing to do
	if picked_up_object == p_object:
		return
	
	# holding something else? drop it
	if picked_up_object:
		drop_object()
	
	# and pick up our new object
	if p_object:
		grab_velocity = global_transform.basis.xform_inv(p_object.linear_velocity - global_velocity)
		#grab_velocity = Vector3.ZERO
		doing_lerp = true
		lerp_time = 0
		picked_up_object = p_object
		picked_up_object.pick_up(self, get_parent())
		emit_signal("has_picked_up", picked_up_object)
		GrabOn()

func _on_button_pressed(p_button):
	#----- pick up behavior --------
	if p_button == pickup_button_id:
		if hovered_object:
			on_HoverEnd(hovered_object)
			hovered_object = null
		if picked_up_object and !picked_up_object.press_to_hold:
			drop_object()
		elif closest_object:
			_pick_up_object(closest_object)
		else: #no object, see if we can move stuff
			if !ScanForObjectToPull():
				doing_pull_search = true
				GrabOn()
				TurnOnPullLaser() #for searching
	
	#----- Interact (point and click) behavior --------        
	elif p_button == action_button_id:
		if picked_up_object and picked_up_object.has_method("action"):
			picked_up_object.action()
		else: #click on hovered
			if picked_up_object:
				var triggerObj = FindParentWithMethod(picked_up_object, "_on_pointer_pressed", 3)
				if triggerObj:
					triggerObj._on_pointer_pressed(hovered_point)
			else:
				if hovered_object:
					if hovered_object.has_method("_on_pointer_pressed"):
						hovered_object._on_pointer_pressed(hovered_point)
					elif hovered_object.get_parent().has_method("_on_pointer_pressed"):
						hovered_object.get_parent()._on_pointer_pressed(hovered_point)
					emit_signal("clicked_on_hovered", hovered_object)

func _on_button_release(p_button):
	#----- pick up behavior --------
	if p_button == pickup_button_id:
		if picked_up_object and picked_up_object.press_to_hold:
			drop_object()
		if doing_pull:
			doing_pull = false
			if pull_object.has_method("DecreasePullCount"):
				pull_object.DecreasePullCount()
				print ("Decrease pull count on ", pull_object.name)            
			pull_object = null
		if doing_pull_search:
			doing_pull_search = false
			TurnOffPullLaser()
			
	#----- Interact (point and click) behavior --------        
	elif p_button == action_button_id:
		pass

func FindParentWithMethod(target, method, depth):
	while target and depth >= 0:
		if target.has_method(method): return target
		target = target.get_parent()
		depth = depth - 1
	return null

func on_HoverStart(obj):
	print ("Started hovering over ", obj.name)
	
	var hObj = FindParentWithMethod(obj, "_on_pointer_hover_start", 2)
	if hObj: hObj._on_pointer_hover_start() #... ***  should provide collided at
	
	if hover_with_laser: TurnOnLaser()
	emit_signal("started_hovering", obj)
	
func on_HoverEnd(obj):
	print ("Stopped hovering over ", obj.name)
	if hover_with_laser: TurnOffLaser()
	
	var hObj = FindParentWithMethod(obj, "_on_pointer_hover_end", 2)
	if hObj: hObj._on_pointer_hover_end() #... ***  should provide collided at
	
	emit_signal("stopped_hovering", obj)

func GrabOn():
	emit_signal("grab_on", picked_up_object)

func GrabOff():
	emit_signal("grab_off", picked_up_object)

func TurnOnPullLaser():
	print ("turn off pickup laser")
	TurnOnLaser()

func TurnOffPullLaser():
	print ("turn off pickup laser")
	TurnOffLaser()

var laser_count = 0
func TurnOnLaser(): #Just make the laser visible
	laser_count += 1
	$Laser.visible = true

func TurnOffLaser():  #turn off laser visibility
	laser_count -= 1
	if laser_count == 0:
		$Laser.visible = false

#return whether we found and started pulling object
func ScanForObjectToPull() -> bool:
	if $RayCast.enabled:
		if $RayCast.is_colliding():
			var obj = $RayCast.get_collider()
			#if obj is RigidBody:
			if obj is Object_pickable_NEW:
				print ("Found object to pull: ", obj.name)
				if obj.mode != RigidBody.MODE_RIGID:
					obj.MakeMoveable()
				#obj.linear_velocity = hull.FloorVelocity(obj.global_transform.origin)
				doing_pull = true
				pull_object = obj
				pull_object_at = $RayCast.get_collision_point()
				if obj.has_method("IncreasePullCount"):
					obj.IncreasePullCount()
					print ("Increase pull count on ", pull_object.name)
				return true
			else: print ("object not Object_pickable ", obj.name)
	return false
	
func ScanForHoverObject() -> bool:
	if $RayCast.enabled:
		if $RayCast.is_colliding():
			var obj = $RayCast.get_collider()
			var triggerObj = FindParentWithMethod(obj, "_on_pointer_pressed", 3)
			if triggerObj:
				if hovered_object and hovered_object != obj:
					#remove hover
					#print ("need to imp hover off")
					on_HoverEnd(hovered_object)
				if obj != hovered_object:
					hovered_object = obj
					hovered_point = $RayCast.get_collision_point()
					on_HoverStart(obj)
				#print ("need to implement hover on!")
				#print ("Found object to hover on: ", hovered_object.name)
				return true
	if hovered_object:
		on_HoverEnd(hovered_object)
		hovered_object = null
	return false
	

func AverageVelocities() -> Vector3:
	var v = Vector3.ZERO
	for i in range(v_sample_size): v += velocity_samples[i]
	v /= v_sample_size
	return v

# final pull of object to grab position.
# Mix position+velocity with straight position lerp
func LerpObject(delta : float):
	lerp_time += delta
	if !picked_up_object or lerp_time >= lerp_max_time:
		doing_lerp = false
	else:
		var a = lerp_time / lerp_max_time
		var pos = lerp(picked_up_object.translation + grab_velocity * delta, \
					lerp_target, sqrt(a))
		picked_up_object.translation = pos


func _process(delta):
	
	_update_closest_object()
	
	if doing_pull_search:
		ScanForObjectToPull()
	elif !doing_lerp and !picked_up_object:
		ScanForHoverObject()
		
	if doing_lerp: LerpObject(delta)
				
	
func _physics_process(delta):
	# update velocity tracking for future use
	global_velocity = (global_transform.origin - last_position) / delta
	last_position = global_transform.origin
	
	velocity_samples[v_sample_current] = global_velocity
	v_sample_current = (v_sample_current+1) % v_sample_size
	
	#for debugging:
	#velocity_in_parent = get_parent().translation - last_parent_pos #<-- NOT proper transform!!
	#last_parent_pos = get_parent().translation
	
	# do force pull on targeted object
	if doing_pull:
		var dir = (global_transform.origin - pull_object.global_transform.origin).normalized()
		var wrong_v = pull_object.linear_velocity - pull_object.linear_velocity.dot(dir) * dir
		#pull_object.apply_central_impulse(dir * pull_power + wrong_v * pull_power2)
		pull_object.apply_central_impulse(dir * pull_power + wrong_v * pull_power2)
		
		print ("pull on ", pull_object.name)
		
		

