extends Node

# TODO!!
#-------
# Lerp size down when inserting
# Lerp size up when removing
# lock down how triggers work
# Hovering should grow the one nearest the hoverer


var inventory : Array = []

class InventoryItem:
	var node
	var temporary : bool = false # if false, then removing will spawn a new thing
	var is_trigger : bool = false
	var originalScale : Vector3
	func _init(newnode : Spatial, temp):
		originalScale = newnode.scale
		temporary = temp
		node = newnode


func AddToInventory(node):
	var item = InventoryItem.new(node, true)
	inventory.push_back(item)
	node.get_parent().remove_child(node)
	add_child(node)

# Remove an item at index in inventory, and give it to controller
func RemoveFromInventory(index, controller):
	if index < 0 || index >= inventory.size():
		print_debug("WARNING! inventory index out of range! ", index)
		return
	var item = inventory[index]
	inventory.remove(index)
