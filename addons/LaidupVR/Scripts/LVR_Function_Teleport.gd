extends KinematicBody
# should really change this to Spatial once #17401 is resolved

# Add this scene as a sub scene of your ARVRController node to implement a teleport function on that controller.

# Reminder, the hierarchy is:
#   ARVROrigin
#     Controller
#       Function: Teleport
#         Teleport <- object that shows the path
#         Target   <- feet indicator + capsule representation
#           Player_figure, Capsule

# Axis 0 = Left/Right on the touchpad
# Axis 1 = Forward/Backward on the touchpad
# Axis 2 = Front trigger
# Button 1 = menu button
# Button 2 = side buttons
# Button 14 = press down on touchpad
# Button 15 = Front trigger (on/off version of Axis 2)

# button 15 is mapped to our trigger
export var teleport_button = 14
export var teleport_axis_x = 0
export var teleport_axis_y = 1
var teleport_button_down : bool = false
var pad_button_down : bool = false
var teleport_down_up_dir : Vector3 #controller y up direction on pressing down to track twisting
var trackpad_down_pos : Vector2

export (float) var click_rotate_degrees = 45 # for click rotate
export (Color) var can_teleport_color = Color(0.0, 1.0, 0.0, 1.0)
export (Color) var cant_teleport_color = Color(1.0, 0.0, 0.0, 1.0)
export (Color) var no_collision_color = Color(45.0 / 255.0, 80.0 / 255.0, 220.0 / 255.0, 1.0)
export var player_height = 1.8 setget set_player_height, get_player_height
export var player_radius = 0.4 setget set_player_radius, get_player_radius
export var strength = 5.0 #how far to cast the path
export var UseCapsuleCast : bool = true
export var TargetOffset : float = .08 #place the feet image this far above the floor hit point
export (NodePath) var Hull # hull must have a hull.radius
export (NodePath) var DirectMover # an LVR_Function_Direct_movement node

# once this is no longer a kinematic body, we'll need this..
# export var collision_mask = 1

onready var ws = ARVRServer.world_scale
var origin_node : Spatial = null
var is_on_floor = true
var show_teleport_controls = false
var can_teleport = true
var teleport_rotation = 0.0; #off default direction
var floor_normal = Vector3(0.0, 1.0, 0.0)
var last_target_transform : Transform = Transform()
var collision_shape = null
var step_size = 0.5 #per step of traversal
var collided_on : Spatial = null
var direct_mover_node

# By default we show a capsule to indicate where the player lands.
# Turn on editable children,
# hide the capsule,
# and add your own player character as child. 
onready var capsule : Spatial = get_node("Target/Player_figure/Capsule")

onready var hull : Spatial = (get_node(Hull) if !Hull.is_empty() else null) # the hull rotates on its Z axis

# -------------------------- setgetters -------------------
func get_player_height():
	return player_height

func set_player_height(p_height):
	player_height = p_height
	
	if collision_shape:
		# for some reason collision shape height measurement is half up, half down from center 
		collision_shape.height = (player_height / 2.0) + 0.1
		
		if capsule:
			capsule.mesh.mid_height = player_height - (2.0 * player_radius)
			capsule.translation = Vector3(0.0, player_height/2.0, 0.0)

func get_player_radius():
	return player_radius

func set_player_radius(p_radius):
	player_radius = p_radius
	
	if collision_shape:
		collision_shape.radius = player_radius

		if capsule:
			capsule.mesh.mid_height = player_height - (2.0 * player_radius)
			capsule.mesh.radius = player_radius

#-------------------------- _ready -----------------------

var controller

func _ready():
	controller = get_parent()
#	if "otherController" in controller && controller.otherController != null:
#		controller = controller.otherController
#		print ("HACK: setting teleport controller to ", controller.name)
	
	if !DirectMover.is_empty(): direct_mover_node = get_node(DirectMover)
	
	if hull != null:
		print ("teleport using hull: ", hull.name)
	else:
		print ("teleport not using a hull!")
		
		#try to autodetect hull, just in case we just forgot to specify
		var pr = get_parent()
		while pr:
			if pr.name == 'hull' || pr.name == "Hull":
				hull = pr
				break
			pr = pr.get_parent()
	
	# We should be a child of an ARVRController which 
	# should be a child of our ARVROrigin, which we assign to origin_node
	origin_node = get_node("../..")

	# It's inactive when we start
	$Teleport.visible = false
	$Target.visible = false
	
	# Scale to our world scale
	$Teleport.mesh.size = Vector2(0.05 * ws, 1.0)
	$Target.mesh.size = Vector2(ws, ws)
	$Target/Player_figure.scale = Vector3(ws, ws, ws)
	
	# create shape object
	collision_shape = CapsuleShape.new()
	
	# call set player to ensure our collision shape is sized
	set_player_height(player_height)
	set_player_radius(player_radius)



func RotateView(degrees):
	var rads = deg2rad(degrees)
	var dp = origin_node.get_node("ARVRCamera").global_transform.origin
	origin_node.rotate_object_local(Vector3.UP, rads)
	dp -= origin_node.get_node("ARVRCamera").global_transform.origin
	origin_node.global_transform.origin += dp

#we try to teleport on pushing the top of the pad down
func ValidTeleport() -> bool:
	if !controller.is_button_pressed(teleport_button):
		teleport_button_down = false
		pad_button_down = false
		return false
	if teleport_axis_x < 0 or teleport_axis_y < 0:
		teleport_button_down = true
		teleport_down_up_dir = controller.transform.basis.xform(transform.basis.xform(Vector3.UP))
		trackpad_down_pos = Vector2.UP
		return true #doesn't use pad
		
	if teleport_button_down: #we only care about position at press down
		return true
		
	var x = controller.get_joystick_axis(teleport_axis_x)
	var y = controller.get_joystick_axis(teleport_axis_y)
	print ("check ",x," ", y)
	if (x > 0 && y > x) or (x<0 && y>-x):
		teleport_button_down = true
		trackpad_down_pos = Vector2(x,y)
		teleport_down_up_dir = controller.transform.basis.xform(transform.basis.xform(Vector3.UP))
		return true
	elif !pad_button_down && x<0 && (y<=-x && y>=x): #click on left part of pad
		pad_button_down = true
		RotateView(click_rotate_degrees)
	elif !pad_button_down && x>0 && (y<=x && y>=-x): #click on right part of pad
		pad_button_down = true
		RotateView(-click_rotate_degrees)
	return false

func _physics_process(delta):
	# We should be a child of the controller on which the teleport is implemented
	
	# check if our world scale has changed..
	var new_ws = ARVRServer.world_scale
	if ws != new_ws:
		ws = new_ws
		$Teleport.mesh.size = Vector2(0.05 * ws, 1.0)
		$Target.mesh.size = Vector2(ws, ws)
		$Target/Player_figure.scale = Vector3(ws, ws, ws)
	
	#print ("teleport button: ", teleport_button, " pressed: ", controller.is_button_pressed(teleport_button))
	if controller and controller.get_is_active() and ValidTeleport():
		collided_on = null;
		if !show_teleport_controls:
			show_teleport_controls = true
			$Teleport.visible = true
			$Target.visible = true
			teleport_rotation = 0.0
		
		# get our physics engine state
		var space = PhysicsServer.body_get_space(self.get_rid())
		var state = PhysicsServer.space_get_direct_state(space)
		var query = PhysicsShapeQueryParameters.new()
		
		# init stuff about our query that doesn't change (note that safe margin and collision_mask
		# need to change once we no longer use kinematic body)
		query.collision_mask = collision_mask
		query.margin = get_safe_margin()
		query.shape_rid = collision_shape.get_rid()
		
		# make a transform for rotating and offseting our shape, it's always lying on its side by default...
		var shape_transform = Transform(Basis(Vector3(1.0, 0.0, 0.0), deg2rad(90.0)),
										Vector3(0.0, player_height / 2.0, 0.0))
		
		# update location..
		var teleport_global_transform = $Teleport.global_transform
		var target_global_origin = teleport_global_transform.origin #this is the spot on the controller
		
		var down_hull_local #hull space downward
		if hull == null: down_hull_local = Vector3(0.0, -1.0 / ws, 0.0)
		else:
			var hullpos = hull.to_local(origin_node.global_transform.origin)
			down_hull_local = Vector3(hullpos.x, hullpos.y, 0).normalized() / ws #hull space
			#down_hull_local = Vector3(origin_node.translation.x, origin_node.translation.y, 0).normalized() / ws #hull space
		
		#print ("origin node position: ", origin_node.translation)
		
		############################################################
		# New teleport logic
		# We're going to use test move in steps to find out where we hit something...
		# This can be optimised loads by determining the length based on the angle 
		# between sections extending the length when we're in a flat part of the arch
		# Where we do get a collision we may want to fine tune the collision
		var cast_length = 0.0
		var fine_tune = 1.0
		var hit_something = false
		var hit_normal : Vector3
		var up_global : Vector3;
		print ("down_hull_local: ", down_hull_local)
		if hull != null: up_global = hull.global_transform.basis.xform(-down_hull_local).normalized()
		else: up_global = origin_node.global_transform.basis.xform(Vector3.UP);
		$Teleport.get_surface_material(0).set_shader_param("up_global", up_global)
		#print ("up_global: ", up_global, "  hull: ", hull.name if hull != null else "null")
		
		var right_global = up_global.cross($Teleport.global_transform.basis.z).normalized()
		var forward_global = up_global.cross(right_global).normalized()
		
		for i in range(1,26):
			#we are throwing out a parabola that is y = -z^2, where y is -up_global,
			#and z perpendicular to y, and is in the plane of Teleport.forward and y
			var new_cast_length = cast_length + (step_size / fine_tune)
			var global_target = Vector3(0.0, 0.0, -new_cast_length) #converted to actual global below
			#global target gets tossed out of z axis of the controller via Teleport object
			
			# our quadratic values
			var t = global_target.z / (strength * ws)
			var t2 = t * t
			
			# target to world space
			global_target = teleport_global_transform.xform(global_target)
			
			# adjust for gravity
			global_target -= up_global * t2
			
			# test our new location for collisions
			#if UseCapsuleCast:
			#   query.transform = Transform(Basis(right_global, up_global, forward_global),
			#                            global_target) \
			#                    * shape_transform
			#   var cast_result = state.collide_shape(query, 10) #returns points of intersection
								
			var cast_result = state.intersect_ray(teleport_global_transform.origin, global_target)
			#state.intersect_ray(global_target)
			
			if cast_result.empty():
				if i == 25: print ("no hit!")
				# we didn't collide with anything.
				# First do a hull check.
				# Otherwise check our next section...
				cast_length = new_cast_length
				target_global_origin = global_target
				
				#check for our cylinder:
				if hull != null:
					var place = hull.to_local(global_target)
					#print ("hull radius: ", hull.radius, " place: ",place)
					if place.x * place.x + place.y * place.y > hull.radius * hull.radius:
						#print ("found a cylinder hit!! ", i)
						if (fine_tune <= 16.0):
							fine_tune *= 2.0
						else:
							is_on_floor = true
							hit_normal = hull.global_transform.basis.xform(Vector3(-place.x, -place.y, 0).normalized())
							floor_normal = hit_normal
							var radiusv = Vector3(place.x, place.y, 0).normalized() * hull.radius
							place = Vector3(radiusv.x, radiusv.y, place.z)
							var collided_at = hull.to_global(place)
							
							cast_length += (collided_at - target_global_origin).length()
							target_global_origin = collided_at
							hit_something = true
							break
				
			elif (fine_tune <= 16.0):
				# try again with a small step size
				fine_tune *= 2.0
			else:
				#if UseCapsuleCast:
				#    print ("Collided: ", cast_result.size(), " points, 1st at: ",cast_result[0], 
				#            " ", to_global(cast_result[0]))
				#else:
				print ("Collided: ", cast_result["collider"].name,
						 "  normal: ", cast_result["normal"],
						 "  position: ", cast_result["position"]
						)
				
				var collided_at = target_global_origin
				if false: #global_target.y > target_global_origin.y: # *** WRANG
					# if we're moving up, we hit the ceiling of something, we don't really care what
					is_on_floor = false
				else:
					# now we cast a ray downwards to see if we're on a surface
					var end_pos = target_global_origin - (up_global * 1)
					print ("down check from ", target_global_origin, " to ", end_pos)
					var intersects = state.intersect_ray(target_global_origin + up_global * .1, end_pos)
					if intersects.empty():
						print ("down check failed")
						is_on_floor = false
						hit_normal = up_global
					else:
						print ("hit! ", intersects["collider"])
						# did we collide with a floor or a wall?
						hit_normal = intersects["normal"]
						floor_normal = intersects["normal"]
						collided_on = intersects["collider"] 
						var dot = floor_normal.dot(up_global)
						if dot > 0.9:
							is_on_floor = true
						else:
							is_on_floor = false
						
						# and return the position at which we intersected
						collided_at = intersects["position"]
				
				# we are colliding, find out if we're colliding on a wall or floor,
				# one we can do, the other nope...
				cast_length += (collided_at - target_global_origin).length()
				target_global_origin = collided_at
				hit_something = true
				break
		
		# and just update our shader
		$Teleport.get_surface_material(0).set_shader_param("scale_t", 1.0 / (strength * ws))
		$Teleport.get_surface_material(0).set_shader_param("ws", ws) #world scale
		$Teleport.get_surface_material(0).set_shader_param("length", cast_length)
		if hit_something:
			var color = can_teleport_color
			#var normal = Vector3(0.0, 1.0, 0.0)
			var normal = hit_normal #in world space
			if is_on_floor:
				# if we're on the floor we'll reorientate our target to match.
				normal = floor_normal
				can_teleport = true
			else:
				can_teleport = false
				color = cant_teleport_color
			#print ("hit normal: ", normal)
			
			# check our axis to see if we need to rotate
			teleport_rotation += (delta * controller.get_joystick_axis(0) * -4.0)
#            var cur_cont_up = controller.transform.basis.xform(transform.basis.xform(Vector3.UP))
#            var cur_cont_fwd = controller.transform.basis.xform(transform.basis.xform(Vector3.FORWARD))
#            var cross = cur_cont_up.normalized().cross(teleport_down_up_dir.normalized())
#            #teleport_rotation = asin(sign(cross.dot(cur_cont_fwd) * cross.length()))
#            teleport_rotation = sign(cross.dot(cur_cont_fwd) * acos(cur_cont_up.dot(teleport_down_up_dir)))
#            print ("rotation: ", teleport_rotation, "  cross: ", cross, "  cont_up: ", cur_cont_up, "  down_up: ", teleport_down_up_dir)
			#teleport_rotation = 0
			
			# update target and colour
			var target_basis = Basis()
#            target_basis.z = Vector3(teleport_global_transform.basis.z.x,
#                                     0.0,
#                                     teleport_global_transform.basis.z.z).normalized()
			target_basis.z = teleport_global_transform.basis.z
			target_basis.y = normal
			target_basis.x = target_basis.y.cross(target_basis.z)
			target_basis.z = target_basis.x.cross(target_basis.y)
			
			#print ("axis: ", normal)
			target_basis = target_basis.rotated(normal, teleport_rotation)
			last_target_transform.basis = target_basis # this is a global transform
			last_target_transform.origin = target_global_origin + TargetOffset * up_global #to be visible
			$Target.global_transform = last_target_transform
#            if hull != null:
#                last_target_transform.basis  \
#                    = Basis(hull.global_transform.basis.xform_inv(last_target_transform.basis.x),
#                            hull.global_transform.basis.xform_inv(last_target_transform.basis.y),
#                            hull.global_transform.basis.xform_inv(last_target_transform.basis.z))
#                last_target_transform.origin = hull.to_local(target_global_origin)
					

			$Teleport.get_surface_material(0).set_shader_param("mix_color", color)
			#$Teleport.get_surface_material(0).set_shader_param("up_global", up_global)
			#$Target.get_surface_material(0).albedo_color = color
			$Target.get_surface_material(0).set_shader_param("OutlineColor", color)
			$Target.visible = can_teleport
		else:
			can_teleport = false
			$Target.visible = false
			$Teleport.get_surface_material(0).set_shader_param("mix_color", no_collision_color)
			
	elif show_teleport_controls:
		if can_teleport:
			# do the actual teleport
			
			# make our target horizontal again
			var new_transform = last_target_transform #a global transform  ****** THIS IS AN OLD TRANSFORM WHEN ROTATING!!
			#new_transform.basis.y = Vector3(0.0, 1.0, 0.0)
			#new_transform.basis.x = new_transform.basis.y.cross(new_transform.basis.z).normalized()
			#new_transform.basis.z = new_transform.basis.x.cross(new_transform.basis.y).normalized()
			
			# find out our user's feet's transformation
			var camera_node = origin_node.get_node("ARVRCamera")
			var cam_transform = camera_node.transform
			var user_feet_transform = Transform()
			user_feet_transform.origin = cam_transform.origin
			user_feet_transform.origin.y = 0 # the feet are on the ground, but have the same X,Z as the camera
			
			# ensure this transform is upright
			user_feet_transform.basis.y = Vector3(0.0, 1.0, 0.0)
			user_feet_transform.basis.x = user_feet_transform.basis.y.cross(cam_transform.basis.z).normalized()
			user_feet_transform.basis.z = user_feet_transform.basis.x.cross(user_feet_transform.basis.y).normalized()
			
			# now move the origin such that the new global user_feet_transform would be == new_transform
			var tr : Transform = new_transform * user_feet_transform.inverse()
			tr = tr.orthonormalized()
 
			if collided_on != null && collided_on != origin_node.get_parent():           
				print ("Changing ovr parent to ", collided_on.name)
				origin_node.get_parent().remove_child(origin_node)
				collided_on.add_child(origin_node)
				origin_node.set_owner(collided_on)
#            -----------------    
#            #special check for if we are hopping onto the monolith.....
#            if collided_on != null: print ("teleport to: ", collided_on.name)
#            if collided_on != null && collided_on.name == "Monolith":
#                origin_node.get_parent().remove_child(origin_node)
#                collided_on.get_parent().add_child(origin_node)
#                origin_node.set_owner(collided_on.get_parent())
#
			origin_node.global_transform = tr
			
			if direct_mover_node: direct_mover_node.UpdateColliderPosition()
		
		# and disable
		show_teleport_controls = false;
		$Teleport.visible = false
		$Target.visible = false

