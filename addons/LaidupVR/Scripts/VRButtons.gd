extends Node

# Standardize pad button events for controllers (like the Vive trackpad)
# Place this script on a child node of your controller

export (bool) var detect_swipes = true setget set_DetectSwipes #true will mean _process is called each frame
export (bool) var emit_pad_values = false setget set_emitPadValues #when non-zero, on process emit new values
export (int) var trackpad_button = 14
export (int) var pad_x_axis = 0
export (int) var pad_y_axis = 1

var controller

# clicking down in corresponding part:
signal PadAny(controller)
signal PadUp(controller)
signal PadDown(controller)
signal PadLeft(controller)
signal PadRight(controller)

# swiping in a direction, without click
signal PadSwipeUp(controller)
signal PadSwipeDown(controller)
signal PadSwipeLeft(controller)
signal PadSwipeRight(controller)
signal PadValues(controller, x, y)

# various state
enum PadDirection { None, Any, Up, Down, Left, Right }
var pad_button_down = PadDirection.None
var down_at : Vector2
var is_swiping : bool = false

func set_DetectSwipes(value):
	detect_swipes = value
	is_swiping = false
	set_process(detect_swipes or emit_pad_values)

func set_emitPadValues(value):
	emit_pad_values = value
	is_swiping = false
	set_process(detect_swipes or emit_pad_values)


func _ready():
	controller = get_parent()
	controller.connect("button_pressed", self, "_on_button_pressed")
	controller.connect("button_release", self, "_on_button_release")
	
	set_process(detect_swipes or emit_pad_values)

func GetSector(x, y):
	if x >= 0:
		if y >= x:
			return PadDirection.Up
		if y <= -x:
			return PadDirection.Down
		return PadDirection.Right
	#else x < 0...
	if y >= -x:
		return PadDirection.Up
	if y <= x:
		return PadDirection.Down
	return PadDirection.Left

func EmitForSwipe(from : Vector2, to : Vector2):
	# ... there could be better gesture detection here, since we are
	# only checking vector between start and end point. anything could have
	# happened in the mean time
	var v = to - from
	var dir = GetSector(v.x, v.y)
	match dir:
		PadDirection.Up:    emit_signal("PadSwipeUp", controller)
		PadDirection.Down:  emit_signal("PadSwipeDown", controller)
		PadDirection.Left:  emit_signal("PadSwipeLeft", controller)
		PadDirection.Right: emit_signal("PadSwipeRight", controller)

	
func ButtonDown():
	is_swiping = false #just terminate any swipe in progress
	down_at = GetPosition()
	pad_button_down = GetSector(down_at.x, down_at.y)
	print ("pad button down: ", pad_button_down)

func ButtonUp():
	if pad_button_down == PadDirection.None:
		return
		
	var curpos  = GetPosition()
	var up_in = GetSector(curpos.x,curpos.y)
	print ("pad button up: ", up_in)
	if pad_button_down == up_in:
		match up_in:
			PadDirection.Up:    emit_signal("PadUp", controller)
			PadDirection.Down:  emit_signal("PadDown", controller)
			PadDirection.Left:  emit_signal("PadLeft", controller)
			PadDirection.Right: emit_signal("PadRight", controller)
	emit_signal("PadAny", controller)
	pad_button_down = PadDirection.None
	
func GetPosition() -> Vector2:
	var v = Vector2.ZERO    
	if pad_x_axis >= 0: v.x = controller.get_joystick_axis(pad_x_axis)
	if pad_y_axis >= 0: v.y = controller.get_joystick_axis(pad_y_axis)
	return v
	
func _on_button_pressed(button):
	if button != trackpad_button: return
	
	if !controller.is_button_pressed(trackpad_button):
		if pad_button_down != PadDirection.None:
			ButtonUp() # catch missing button up event
			return
	ButtonDown()

func _on_button_release(button):
	if button != trackpad_button: return
	
	if pad_button_down != PadDirection.None:
		ButtonUp()
	
func _process(delta):
	if pad_button_down != PadDirection.None: return
	
	var v2 = GetPosition()
	if v2.x == 0 && v2.y == 0:
		if !is_swiping: return
		# we are done swiping
		EmitForSwipe(down_at, v2)
		is_swiping = false
		return
		
	# else: start or update a swipe
	if is_swiping: #update swipe
		if emit_pad_values:
			emit_signal("PadValues", controller, v2.x, v2.y)
	else: # start swipe
		is_swiping = true
		down_at = v2
	
	
	
	
