"""
VRHoverArea.gd

Place this script on an Area connected to a vr controller.
When this Area collides with other areas, and the collider has
a script vr_hover_enter(area) or vr_hover_exit(area), they will be
called. If the node doesn't have those functions, its parents are checked.

This node also maintains an array of objects it is hovering over in self.hovering.
"""

extends Area

signal hover_enter(obj)
signal hover_leave(obj)
signal hover_closest(obj, distance) #Emits when the closest hovered object changes

export (bool) var detect_closest : bool = true
export (NodePath) var proxySource
var source = null

var last_closest
var hovering = []

var ignore_hover : bool = false setget SetIgnore # ignore enters, but still process exits?
func SetIgnore(value):
	ignore_hover = value
	if ignore_hover && hovering.size() > 0:
		while hovering.size() > 0:
			_on_HoverSphere_area_exited(hovering[hovering.size()-1])


func _ready():
	if proxySource != null && proxySource != "": source = get_node(proxySource)
	else: source = self
	
	connect("area_entered", self, "_on_HoverSphere_area_entered")
	connect("area_exited", self, "_on_HoverSphere_area_exited")

var ws = 0

func apply_world_scale():
	var new_ws = ARVRServer.world_scale
	if (ws != new_ws):
		ws = new_ws
		scale = Vector3(ws, ws, ws)

func _process(delta):
	apply_world_scale()

#--------------------- HoverSphere interactions -------------------

func _on_HoverSphere_area_entered(area):
	if ignore_hover: return
	print ("Area entered hoverSphere: ", area.name)
	
	if hovering.find(area) < 0:
		hovering.push_back(area)
		UpdateClosest()
	emit_signal("hover_enter", area)
	
	if area.has_method("vr_hover_enter"): 
		#print ("area has vr_hover_enter")
		area.vr_hover_enter(area, source)
		return
		
	var pr = area.get_parent()
	while pr != null:
		if pr.has_method("vr_hover_enter"): 
			pr.vr_hover_enter(area, source)
			return
		pr = pr.get_parent()

func _on_HoverSphere_area_exited(area):
	print ("Area exited hoverSphere: ", area.name)

	emit_signal("hover_leave", area)
	if hovering.find(area) >= 0:
		hovering.erase(area)
		UpdateClosest()

	if area.has_method("vr_hover_exit"): 
		#print ("area has vr_hover_enter")
		area.vr_hover_exit(area, source)
		return
		
	var pr = area.get_parent()
	while pr != null:
		if pr.has_method("vr_hover_exit"): 
			pr.vr_hover_exit(area, source)
			return
		pr = pr.get_parent()

func _on_HoverSphere_body_entered(body):
	print ("Body entered hoverSphere: ", body.name)

func _on_HoverSphere_body_exited(body):
	print ("Body exited hoverSphere: ", body.name)


func UpdateClosest():
	if !detect_closest: return
	var closest
	var dist = 100000.0
	var d
	for o in hovering:
		d = (o.global_transform.origin - global_transform.origin).length()
		if d < dist:
			dist = d
			closest = o
	
	if closest != last_closest:
		emit_signal("hover_closest", closest, dist)
		last_closest = closest


