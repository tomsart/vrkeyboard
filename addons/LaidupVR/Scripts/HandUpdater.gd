extends Spatial

#
# Node to respond to hover, point, and grab signals from a LVR_Function_Pickup
# Put this on an ARVR controller. Will autodetect a sibling LVR_Function_Pickup.
#

#vive buttons for reference:
# Axis 0 = Left/Right on the touchpad
# Axis 1 = Forward/Backward on the touchpad
# Axis 2 = Front trigger
# Button 1 = menu button
# Button 2 = side buttons
# Button 14 = press down on touchpad
# Button 15 = Front trigger (on/off version of Axis 2)

export (String) var IdleAnimation = "Hand Idle"
export (String) var HoverAnimation = "Hand WideOpen"
export (String) var GrabAnimation = "Hand Grab"
export (String) var PointAnimation = "Hand IndexPoint"
export (String) var ClickAnimation = "Hand IndexClick"

export (bool) var is_left : bool = false
export (bool) var append_R_or_L : bool = true

export (int) var button_point = 15
export (int) var button_grab = 2
export (int) var button_trackpad = 14
export (bool) var use_internal_signals : bool = false

onready var animator : AnimationPlayer = get_node("AnimationPlayer")
onready var controller : ARVRController = get_parent()
onready var state_machine = $AnimationTree.get("parameters/playback")

var fingerCurl : Array = [0,0,0,0,0]


enum HandState {
	Idle,
	Hovering,
	Grabbing,
	Pointing,
	PointClick,
	IndexClenched,
	ThreeClenched,
	Fist,
	ThumbsUp
   }

func _ready():
	if use_internal_signals:
		controller.connect("button_pressed", self, "ButtonPressed")
		controller.connect("button_release", self, "ButtonReleased")
	else:
		ConnectSignals()
	
	if append_R_or_L:
		var append = ".L" if is_left else ".R"
		IdleAnimation  = IdleAnimation  + append
		HoverAnimation = HoverAnimation + append
		GrabAnimation  = GrabAnimation  + append
		PointAnimation = PointAnimation + append
		ClickAnimation = ClickAnimation + append
	Idle()


func ButtonPressed(button):
	print ("Button on ",controller.get_controller_name(), ": ", button)
	if (button == button_point):
		Point()
	elif (button == button_grab):
		Grab()

func ButtonReleased(button):
	print ("Button off ",controller.get_controller_name(), ": ", button)
	Idle()

func SetFingerCurl(finger, curl):
	#only useful for index controllers or other full hand trackers
	print ("todo: Set individual curl")
	if finger < 0 || finger >=5:
		push_error("Bad finger value!")
		return
	fingerCurl[finger] = curl

func PointClick():
	print ("Click animation!")
	#animator.play("Hand IndexClick.R")
	state_machine.travel(ClickAnimation)

func Point():
	print ("Point animation!")
	#animator.play("Hand IndexPoint.R")
	state_machine.travel(PointAnimation)
	
func Idle():
	#animator.play("Hand Idle.R")
	state_machine.travel(IdleAnimation)

func HoverHand():
	#animator.play("Hand WideOpen.R")
	state_machine.travel(HoverAnimation)

func Grab():
	print ("Grab animation!")
	#animator.play("Hand Grab.R")
	state_machine.travel(GrabAnimation)


func ConnectSignals():
	var found = false
	for child in get_parent().get_children():
		if child is LVR_Function_Pickup:
			child.connect("grab_off", self, "_on_FunctionPickup2_grab_off")
			child.connect("grab_on",  self, "_on_FunctionPickup2_grab_on")
			child.connect("started_hovering", self, "_on_FunctionPickup2_started_hovering")
			child.connect("stopped_hovering", self, "_on_FunctionPickup2_stopped_hovering")
			child.connect("clicked_on_hovered", self, "_on_FunctionPickup2_clicked_on_hovered")
			found = true
			break
	print ("ConnectSignals: ", found)


#----- *** should really try to figure out the anim tree thing:
func _on_FunctionPickup2_grab_off(obj):
	Idle()

func _on_FunctionPickup2_grab_on(obj):
	Grab()

func _on_FunctionPickup2_started_hovering(obj):
	Point()

func _on_FunctionPickup2_stopped_hovering(obj):
	Idle()

func _on_FunctionPickup2_clicked_on_hovered(obj):
	PointClick()
