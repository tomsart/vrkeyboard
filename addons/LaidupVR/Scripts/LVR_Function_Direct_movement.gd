extends Node

enum MOVEMENT_TYPE { MOVE_AND_ROTATE, MOVE_AND_STRAFE }

var hull : Spatial
var hull_radius : float = 0

export (bool) var move_active : bool = true # toggle to disable whenever you want

# We don't know the name of the camera node... 
export (NodePath) var camera = null

# size of our player
export var player_radius = 0.4 setget set_player_radius, get_player_radius

# to combat motion sickness we'll 'step' our left/right turning
export var turn_delay = 0.2
export var turn_angle = 20.0
export var max_speed = 50.0
export var sprint = 10.0
export var sprint_button = 14
export var drag_factor = 0.1
export var use_drag : bool = true

# fly mode and strafe movement management
export(MOVEMENT_TYPE) var move_type = MOVEMENT_TYPE.MOVE_AND_ROTATE
export var canFly = true
export var fly_move_button_id = 15
export var fly_activate_button_id = 2
var isflying = false

var turn_step = 0.0
var origin_node = null
var camera_node = null
var velocity = Vector3(0.0, 0.0, 0.0) # global space
export (bool) var lock_y = false
export (bool) var use_gravity = true
export (Vector3) var gravity_dir = Vector3(0,-30,0)
onready var collision_shape = get_node("KinematicBody/CollisionShape")
onready var tail = get_node("KinematicBody/Tail")

func get_player_radius():
	return player_radius

func set_player_radius(p_radius):
	player_radius = p_radius

func set_velocity(v):
	velocity = v

func _ready():
	# origin node should always be the parent of our parent
	origin_node = get_node("../..")
	
	if camera:
		camera_node = get_node(camera)
	else:
		# see if we can find our default
		camera_node = origin_node.get_node('ARVRCamera')
	
	set_player_radius(player_radius)
	
	#try to autodetect hull, just in case we just forgot to specify
	var pr = get_parent()
	while pr:
		#if pr is HullTilted || pr.name == 'hull' || pr.name == "Hull":
		if pr.name == 'hull' || pr.name == "Hull":
			hull = pr
			#if hull is HullTilted: check_hull_collision = false
			break
		pr = pr.get_parent()
	
	if hull != null: print ("Teleport ready Found hull: ", hull.name)
	else: print ("Teleport ready Found no hull!")
	
	
	if hull:
		var global_up : Vector3
		if hull.which_axis == LVR_Defs.RotationAxis.Z:
			global_up = Vector3(origin_node.global_transform.origin.x, origin_node.global_transform.origin.y, 0)
			hull_radius = global_up.length()
		else:
			global_up = Vector3(0, origin_node.global_transform.origin.y, origin_node.global_transform.origin.z)
			hull_radius = global_up.length()
	print ("init direct move found hull radius: ", hull_radius)


func _physics_process(delta):
	if !move_active: # whether to move at all, not if there is currently any move input
		return
		
	if !origin_node:
		return
	
	if !camera_node:
		return
	
	# Set our collision object position to be centered around camera, and squish according to camera height
	var camera_height = camera_node.transform.origin.y #local y
	if camera_height < player_radius:
		# not smaller than this
		camera_height = player_radius
	
	collision_shape.shape.radius = player_radius
	collision_shape.shape.height = camera_height - player_radius
	collision_shape.transform.origin.y = (camera_height / 2.0) + player_radius
	# note: x and z remain where they were
	
	# We should be the child or the controller on which the teleport is implemented
	var controller = get_parent()
	if controller.get_is_active():
		var left_right = controller.get_joystick_axis(0)
		var forwards_backwards = controller.get_joystick_axis(1)
		
		if left_right == 0 && forwards_backwards == 0 && velocity.length() < .001 \
				&& !use_gravity:
			# print ("no move and no gravity, skipping")
			return
		#print ("move: ", left_right, ", ", forwards_backwards)
		
		# if fly_action_button_id is pressed it activates the FLY MODE
		# if fly_action_button_id is released it deactivates the FLY MODE
		if controller.is_button_pressed(fly_activate_button_id) && canFly:
			isflying =  true
		else:
			isflying = false
		
		var tmax_speed = max_speed
		if controller.is_button_pressed(sprint_button):
			tmax_speed *= sprint
			
		
		# if player is flying, he moves following the controller's orientation
		if isflying:
			print ("flying...")
			if controller.is_button_pressed(fly_move_button_id):
				# is flying, so we will use the controller's transform to move the VR capsule follow its orientation					
				var curr_transform = $KinematicBody.global_transform
				velocity = controller.global_transform.basis.z.normalized() * -delta * tmax_speed * ARVRServer.world_scale
				velocity = $KinematicBody.move_and_slide(velocity)
				var movement = ($KinematicBody.global_transform.origin - curr_transform.origin)
				origin_node.global_transform.origin += movement
		
		################################################################
		# first process turning, no problems there :)
		# move_type == MOVEMENT_TYPE.move_and_strafe
		else:
			if(move_type == MOVEMENT_TYPE.MOVE_AND_ROTATE && abs(left_right) > 0.1):
				if left_right > 0.0:
					if turn_step < 0.0:
						# reset step
						turn_step = 0
				
					turn_step += left_right * delta
				else:
					if turn_step > 0.0:
						# reset step
						turn_step = 0
				
					turn_step += left_right * delta
			
				if abs(turn_step) > turn_delay:
					# we rotate around our Camera, but we adjust our origin, so we need a little bit of trickery
					var t1 = Transform()
					var t2 = Transform()
					var rot = Transform()
				
					t1.origin = -camera_node.transform.origin
					t2.origin = camera_node.transform.origin
				
					# Rotating
					while abs(turn_step) > turn_delay:
						if (turn_step > 0.0):
							rot = rot.rotated(Vector3(0.0,-1.0,0.0),turn_angle * PI / 180.0)
							turn_step -= turn_delay
						else:
							rot = rot.rotated(Vector3(0.0,1.0,0.0),turn_angle * PI / 180.0)
							turn_step += turn_delay
				
					origin_node.transform *= t2 * rot * t1
					
			else: # not rotating
				turn_step = 0.0
		
			################################################################
			# now we do our movement
			# We start with placing our KinematicBody in the right place
			# by centering it on the camera but placing it on the ground
			var kb_transform_global = $KinematicBody.global_transform
			var camera_transform_global = camera_node.global_transform
			kb_transform_global.origin = camera_transform_global.origin
			kb_transform_global.basis = camera_transform_global.basis
			
			var global_up : Vector3 = Vector3(0,1,0)
			if hull: # need up for simplistic fake gravity
				if hull.which_axis == LVR_Defs.RotationAxis.Z:
					global_up = Vector3(kb_transform_global.origin.x, kb_transform_global.origin.y, 0)
				else:
					global_up = Vector3(0, kb_transform_global.origin.y, kb_transform_global.origin.z)
				#var cur_radius = global_up.length()
				global_up = global_up.normalized()
			else:
				kb_transform_global.origin -= camera_node.transform.origin.y * global_up  # supposed to put on ground
			$KinematicBody.global_transform = kb_transform_global
			
			# Apply our drag
			if use_drag:
				velocity *= (1.0 - drag_factor)
				if left_right == 0 && forwards_backwards == 0: #damp more when not touching
					velocity.x *= .9
			
			if move_type == MOVEMENT_TYPE.MOVE_AND_ROTATE:
				if (abs(forwards_backwards) > 0.1 ): #and tail.is_colliding()):
					var dir = camera_transform_global.basis.z
					# dir.y = 0.0  *** FIXME! Set to plane of origin_node
					velocity = dir.normalized() * -forwards_backwards * delta * tmax_speed * ARVRServer.world_scale
					#velocity = velocity.linear_interpolate(dir, delta * 100.0)		
					
			elif move_type == MOVEMENT_TYPE.MOVE_AND_STRAFE:
				if ((abs(forwards_backwards) > 0.1 ||  abs(left_right) > 0.1)): # and tail.is_colliding()):
					var dir_forward = camera_transform_global.basis.z
					# dir_forward.y = 0.0 *** FIXME! Set to plane of origin_node
					# VR Capsule will strafe left and right
					var dir_right = camera_transform_global.basis.x;
					# dir_right.y = 0.0
					# print ("forward: ", dir_forward, "  right: ", dir_right)
					velocity += (dir_forward * -forwards_backwards + dir_right * left_right).normalized() * delta * tmax_speed * ARVRServer.world_scale
			
			if use_gravity:
				velocity += delta * gravity_dir

			# apply move and slide to our kinematic body
			# print ("velocity before move ", velocity)
			#var oldpos = $KinematicBody.global_transform.origin
			velocity = $KinematicBody.move_and_slide(velocity, Vector3(0,0,0)) #global_up)
			# print ("velocity after move ", velocity) #, "   moved: ", $KinematicBody.global_transform.origin - oldpos)
			
#			# apply our gravity
#			if gravity != 0:
#				var gravity_velocity = -gravity * global_up # Vector3(0.0, velocity.y, 0.0)
#
#				gravity_velocity.y += gravity * delta
#				gravity_velocity = $KinematicBody.move_and_slide(gravity_velocity, Vector3(0.0, 1.0, 0.0))
#				velocity.y = gravity_velocity.y
			
			# now use our new position to move our origin point
			var movement = ($KinematicBody.global_transform.origin - kb_transform_global.origin)
			# print ("movement: (", movement.length(),") ", movement)
			if lock_y: movement.y = 0
			var new_origin = origin_node.global_transform.origin + movement
			
			# Return this back to where it was so we can use its collision shape for other things too
			# $KinematicBody.global_transform.origin = curr_transform.origin
			
			# correct position and rotation for hull_radius within rotating hull
			if hull:
				if hull.which_axis == LVR_Defs.RotationAxis.Z:
					global_up = Vector3(new_origin.x, new_origin.y, 0)
				else:
					global_up = Vector3(0, new_origin.y, new_origin.z)
				
				var cur_radius = global_up.length()
				global_up = global_up.normalized()
				var vv = global_up * hull_radius
				
				# set new origin_node origin
				if hull.which_axis == LVR_Defs.RotationAxis.Z:
					new_origin = Vector3(vv.x, vv.y, new_origin.z)
				else:
					new_origin = Vector3(new_origin.x, vv.y, vv.z)
				origin_node.global_transform.origin = new_origin
				
				# set new origin_node basis
				var xx = origin_node.global_transform.basis.x
				var zz = xx.cross(-global_up)
				xx = -global_up.cross(zz)
				var new_basis = Basis(xx, -global_up, zz).orthonormalized()
				origin_node.global_transform.basis = new_basis
				
			else:
				# no hull to worry about, just straight assign
				origin_node.global_transform.origin = new_origin
			
			UpdateColliderPosition()
#			kb_transform_global.basis = origin_node.global_transform.basis
#			kb_transform_global.origin = 
#			$KinematicBody.global_transform.basis = origin_node.global_transform.basis
#			#$KinematicBody.global_transform = kb_transform_global

# Used by teleport to make sure our collider stays in an appropriate position
func UpdateColliderPosition():
	var kb_transform_global = $KinematicBody.global_transform
	#var target_radius = hull_radius - collision_shape.transform.origin.y
	
	var global_up : Vector3 = Vector3(0,1,0)
	if hull: # need up for simplistic fake gravity
		if hull.which_axis == LVR_Defs.RotationAxis.Z:
			global_up = Vector3(kb_transform_global.origin.x, kb_transform_global.origin.y, 0)
		else:
			global_up = Vector3(0, kb_transform_global.origin.y, kb_transform_global.origin.z)
		#var cur_radius = global_up.length()
		global_up = global_up.normalized()
		kb_transform_global.origin = camera_node.global_transform.origin + camera_node.transform.origin.y/2 * global_up
	else:
		kb_transform_global.origin = camera_node.global_transform.origin - camera_node.transform.origin.y/2 * global_up
	
	kb_transform_global.basis = origin_node.global_transform.basis
	$KinematicBody.global_transform = kb_transform_global
	
	
