extends RigidBody

class_name Object_pickable_NEW

# Set hold mode
export (bool) var press_to_hold = true
export (int, FLAGS, "layer_1", "layer_2", "layer_3", "layer_4", "layer_5") var picked_up_layer = 0
export (NodePath) var pointClickNode;
#export (bool) var return_to_original = false #when dropped, does object revert to original pos

# Remember some state so we can return to it when the user drops the object
onready var original_parent = get_parent()
onready var original_position = translation
onready var floating_parent = get_parent()
onready var original_collision_mask = collision_mask
onready var original_collision_layer = collision_layer

# Who picked us up?
var picked_up_by = null
var by_controller : ARVRController = null
var closest_count = 0
var being_pulled_count = 0
var pointerNode

func IsHeld(): return picked_up_by != null

func _ready():
	print ("Object_pickable_NEW ready!")
	if pointClickNode.is_empty():
		if has_node("PointerInteraction"):
			pointerNode = get_node("PointerInteraction")
			print ("Assigned PointerInteraction node on ", name)
	else: pointerNode = get_node(pointClickNode)
	

func _on_pointer_pressed(collided_at):
	print ("pressed on ", name)
	if pointerNode:
		pointerNode._on_pointer_pressed(collided_at)

func _input(event):
	if event is InputEventKey:
		print("Key down:", event.scancode, event.get_scancode_with_modifiers())
		if event.scancode == 32:
			if picked_up_by: let_go()
			mode = RigidBody.MODE_KINEMATIC
			get_parent().remove_child(self)
			original_parent.add_child(self)
			translation = original_position
			
	
# Are we currently being held by something?
func is_picked_up():
	if picked_up_by:
		return true
	return false

func _update_highlight():
	# should probably implement this in our subclass
	pass

func IncreasePullCount():
	being_pulled_count += 1

func DecreasePullCount():
	being_pulled_count -= 1

func increase_is_closest():
	closest_count += 1
	_update_highlight()

func decrease_is_closest():
	closest_count -= 1
	_update_highlight()

# Drop object, then destroy object after dropped. Default destroy immediately.
func drop_and_free():
	if picked_up_by:
		picked_up_by.drop_object()
	
	queue_free()

# we are being picked up by, which is associated with with_controller.
# Reparent ourselves to by.
func pick_up(by, with_controller):
	if picked_up_by == by:
		return
	
	if picked_up_by: #held by something else!
		let_go()
	
	# remember who picked us up
	picked_up_by = by
	by_controller = with_controller
	
	# turn off physics on our pickable object
	mode = RigidBody.MODE_KINEMATIC
	collision_layer = picked_up_layer
	collision_mask = 0
	
	# now reparent it
	var tr = global_transform
	get_parent().remove_child(self)
	picked_up_by.add_child(self)
	
	# reset our transform
	#transform = Transform() ...don't reset, Function_Pickup will lerp it into place
	global_transform = tr #is this necessary since we noop tr?

# we are being let go
func let_go(impulse = Vector3(0.0, 0.0, 0.0)):
	print ("object_pickable let_go()... was picked_up_by: ", picked_up_by)
	if picked_up_by:
		# get our current global transform
		var t = global_transform
		
		# reparent it
		picked_up_by.remove_child(self)
		floating_parent.add_child(self)
		#original_parent.add_child(self)
		
		# reposition it and apply impulse
		global_transform = t
		mode = RigidBody.MODE_RIGID
		collision_mask = original_collision_mask
		collision_layer = original_collision_layer
		apply_impulse(Vector3(0.0, 0.0, 0.0), impulse)
		
		# we are no longer picked up
		picked_up_by = null
		by_controller = null

# Convert state of ourselves to free floating rigid bodies
func MakeMoveable():
	if mode == RigidBody.MODE_RIGID: return #assume already there if tagged as full rigid
	
	mode = RigidBody.MODE_RIGID
	var tr = global_transform
	get_parent().remove_child(self)
	floating_parent.add_child(self)
	global_transform = tr
	linear_velocity = Vector3.ZERO
	#linear_velocity = hull.FloorVelocity(obj.global_transform.origin)
					
					
