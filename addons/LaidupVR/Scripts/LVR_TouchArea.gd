"""
VRTouchArea.gd

Place this script on an Area connected to a vr controller.
When this Area collides with other areas, and the collider has
a script vr_touch_enter(area, source) or vr_touch_exit(area, source), they will be
called. If the node doesn't have those functions, its parents are checked.

Also maintains arrays touching, grabbing, and triggering to store what
is currently intersecting, what was intersecting when the grab button was pressed,
and what was intersecting when the trigger button was pressed. grabbing and triggering
are both cleared on button up, but note that by then they may not have been
intersecting.
"""

extends Area

signal touch_enter(obj)
signal touch_leave(obj)

export (NodePath) var proxySource
export (int) var grabButton = 2   # set to -1 if you don't want to check for this
export (int) var triggerButton = 15 # set to -1 if you don't want to check for this

var ignore_hover : bool = false setget SetIgnore # ignore enters, but still process exits?
func SetIgnore(value):
	ignore_hover = value
	if ignore_hover && touching.size() > 0:
		while touching.size() > 0:
			_on_TouchSphere_area_exited(touching[touching.size()-1])

var source = null
var touching = []
var grabbing = []
var triggering = []

var gbutton_is_down : bool = false
var tbutton_is_down : bool = false

var ws = 0

func apply_world_scale():
	var new_ws = ARVRServer.world_scale
	if (ws != new_ws):
		ws = new_ws
		scale = Vector3(ws, ws, ws)


func _process(delta):
	apply_world_scale()


func _ready():
	if proxySource != null && proxySource != "": source = get_node(proxySource)
	else: source = self
	connect("area_entered", self, "_on_TouchSphere_area_entered")
	connect("area_exited", self, "_on_TouchSphere_area_exited")
	
	#get_parent().connect("button_pressed", self, "_on_button_pressed")
	#get_parent().connect("button_release", self, "_on_button_release")
	
	get_parent().connect("controller_activated", self, "_on_controller_activated")
	
	#name = get_parent().name+"_"+name

func _on_controller_activated(controller):
	print ("Controller activated: ", controller.name)
	get_parent().connect("button_pressed", self, "_on_button_pressed")
	get_parent().connect("button_release", self, "_on_button_release")


#--------------------- TouchSphere interactions -------------------
func _on_TouchSphere_area_entered(area : Area):
	if ignore_hover: return
	print ("Area entered TouchSphere: ", area.name)
	
	if touching.find(area) < 0: touching.push_back(area)
	
	emit_signal("touch_enter", area)
	
	if area.has_method("vr_touch_enter"): 
		#print ("area has vr_touch_enter")
		area.vr_touch_enter(area, source)
		return
		
	var pr = area.get_parent()
	while pr != null:
		if pr.has_method("vr_touch_enter"): 
			pr.vr_touch_enter(area, source)
			return
		pr = pr.get_parent()
		
func _on_TouchSphere_area_exited(area):
	print ("Area exited TouchSphere: ", area.name)
	
	if touching.find(area) >= 0: touching.erase(area)
	
	emit_signal("touch_leave", area)
	
	if area.has_method("vr_touch_exit"): 
		#print ("area has vr_touch_enter")
		area.vr_touch_exit(area, source)
		return
		
	var pr = area.get_parent()
	while pr != null:
		if pr.has_method("vr_touch_exit"): 
			pr.vr_touch_exit(area, source)
			return
		pr = pr.get_parent()
	
	
func _on_button_pressed(button):
	print (get_parent().name,".",name, " controller button press: ", button, ", grab: ", button == grabButton)
	if button == grabButton:
		gbutton_is_down = true
		for o in touching:
			var pr = o
			while pr != null:
				if pr.has_method("vr_touch_grab"): 
					print ("pr with method vr_touch_grab: ", pr.name)
					pr.vr_touch_grab(get_parent())
					grabbing.append(pr)
					break
				#else: print ("pr WITHOUT method vr_touch_grab: ", pr.name)
				pr = pr.get_parent()
				
	elif button == triggerButton:
		tbutton_is_down = true
		for o in touching:
			var pr = o
			while pr != null:
				if pr.has_method("vr_touch_trigger"): 
					#print ("pr with method vr_touch_trigger: ", pr.name)
					pr.vr_touch_trigger(get_parent())
					triggering.append(pr)
					break
				#else: print ("pr WITHOUT method vr_touch_trigger: ", pr.name)
				pr = pr.get_parent()
			

func _on_button_release(button):
	print (get_parent().name,".",name, " controller button release: ", button, ", grab: ", button == grabButton)
	if button == grabButton:
		gbutton_is_down = false
		for o in grabbing:
			if o.has_method("vr_touch_ungrab"): 
				o.vr_touch_ungrab(get_parent())
		grabbing = []
		
	elif button == triggerButton:
		tbutton_is_down = false
		for o in triggering:
			if o.has_method("vr_touch_untrigger"): 
				o.vr_touch_untrigger(get_parent())
		triggering = []

func _on_TouchSphere_body_entered(body):
	print ("Body entered TouchSphere: ", body.name)

func _on_TouchSphere_body_exited(body):
	print ("Body exited TouchSphere: ", body.name)

