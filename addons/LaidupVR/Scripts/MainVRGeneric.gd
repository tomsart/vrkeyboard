#
# Overall game control, like quiting
#

extends Node

export (NodePath) var FPSController
export (NodePath) var VRController  #The OVRFirstPerson node

export (bool) var no_vr : bool = false

func _ready():
	print ("Start!")
	#--------- Set up for vr --------------
	var arvr_interface = ARVRServer.find_interface("OpenVR")
	if !no_vr and arvr_interface and arvr_interface.initialize():
		print ("VR found! Woo!")
		# switch to ARVR mode
		get_viewport().arvr = true
		
		# keep linear color space, not needed with the GLES2 renderer
		get_viewport().keep_3d_linear = false
		#get_viewport().keep_3d_linear = true  #<- makes it not like viewport 
		
		# make sure vsync is disabled or we'll be limited to 60fps
		OS.vsync_enabled = false
		
		# up our physics to 90fps to get in sync with our rendering
		Engine.target_fps = 90
		
		#turn off fps:
		if FPSController:
			print ("Removing normal 1st person controller")
			var fpc = get_node(FPSController)
			fpc.get_parent().remove_child(fpc)
		
	else:
		print ("TODO!!! VR not found, setting up for non-vr normal 1st person controller")
		if VRController:
			print ("Removing vr controller")
			var vrc = get_node(VRController)
			vrc.get_parent().remove_child(vrc)
	
	# just for testing, list what models are available
	# var ovr_model = preload("res://addons/godot-openvr/OpenVRRenderModel.gdns").new()
	# var model_names = ovr_model.model_names()
	# print("models: " + str(model_names))



func _input(event):
	if event is InputEventKey:
		#print ("event: ", event)
		if event.scancode == KEY_ESCAPE:
			get_tree().quit()
#    if Input.is_action_pressed("quit"):
#        get_tree().quit()


func _on_TouchSphere_body_entered(body):
	print("body entered: ", body.name)

func _on_TouchSphere_area_entered(area):
	print("area entered: ", area.name)

