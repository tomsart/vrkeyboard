extends Object
class_name UtilFuncs

#
# A collection of random useful functions.
#


# Return an object that has method, else null.
# Checks obj for method, and each subsequent parent.
func FindParentWithMethod(obj, method : String):
	var pr = obj
	while pr != null:
		if pr.has_method(method): 
			#print ("pr with method ", method, ": ", pr.name)
			return pr
		#else: print ("pr WITHOUT method vr_touch_grab: ", pr.name)
		pr = pr.get_parent()
	return null

# Find the parent XRController, or null if there isn't one.
func FindParentController(obj) -> ARVRController:
	var pr = obj
	while pr != null:
		if pr is ARVRController: 
			return pr
		pr = pr.get_parent()
	return null
