extends Spatial

#
# OrbitCamera.gd
#

export(int) var ORBIT_BUTTON = 3 #the middle
export(int) var PAN_BUTTON = 2 #right button
var WHEEL_UP = 4
var WHEEL_DOWN = 5
export(float) var orbit_dist : float = 0
export(float) var pan_speed : float = 20
#export(bool) var always_up : bool = true
export(NodePath) var LookAt : NodePath


var camera : Spatial

var mbdown = false
var lbdown = false
var pan_active = false

var click_point : Vector2
var prev_point : Vector2

var look_at : Spatial
var look_at_pos : Vector3 = Vector3.ZERO

func _ready():
	if !LookAt.is_empty():
		look_at = get_node(LookAt)
		look_at_pos = look_at.global_transform.origin
	
	camera = self
	if orbit_dist == 0: 
		#orbit_dist = camera.translation.length()
		orbit_dist = (camera.global_transform.origin - look_at_pos).length()
	

func _input(event):
	if (event is InputEventMouseButton):
#		print("Mouse Click/Unclick at: ",event.position, ", pressed: ", event.pressed, ", button: ", event.button_index)
		if event.pressed: 
			if event.button_index == ORBIT_BUTTON:
				mbdown = true
				click_point = event.position
				prev_point = click_point
				
			elif event.button_index == WHEEL_UP:
				var dist = camera.translation.length() * 1/1.1
				camera.translation = camera.translation.normalized() * dist
				
			elif event.button_index == WHEEL_DOWN:
				var dist = camera.translation.length() * 1.1
				camera.translation = camera.translation.normalized() * dist
				
			elif event.button_index == PAN_BUTTON:
				pan_active = true
				click_point = event.position
				prev_point = click_point
			
		else:
			if event.button_index == ORBIT_BUTTON:
				mbdown = false
			elif event.button_index == PAN_BUTTON:
				pan_active = false
	
	elif (event is InputEventMouseMotion):
		#print("Mouse move: ",event.relative.x, " ", event.relative.y, " ", yaw)
		prev_point = click_point
		click_point = event.position
		
		if mbdown:
			var dm = .01 * (click_point - prev_point)
			var dist = camera.translation.length()
			camera.translation += -pan_speed * dm.x * camera.transform.basis.x + pan_speed * dm.y * camera.transform.basis.y
			camera.translation = camera.translation.normalized() * dist
			camera.look_at(look_at_pos, Vector3(0,1,0))
			
		if pan_active: #pan camera
			# *** FINISH ME!!! need to coordinate with orbit
			var dm = .01 * (click_point - prev_point)
			camera.rotate_object_local(Vector3(1,0,0), dm.y)
			camera.rotate(Vector3(0,1,0), dm.x)


