class_name VectorUtils

#
# Note:
#
#   v1.project(v2)      == part of v1 parallel to v2
#   v1 - v1.project(v2) == part of v1 perpendicular to v2
#
#

# a||b = the part of a that is parallel to b. NOTE: this is the same as a.project(b)
func a_parallel_to(a : Vector3, b : Vector3):
	return a.project(b)
	#return ((a.dot(b)/(b.length_squared()))*b);

#//! a|=b = the part of a that is perpendicular to b
func a_perpendicular_to(a : Vector3, b : Vector3):
	var c = a_parallel_to(a,b)
	return (a-c);


# align basis from to basis to, preserving original scale
func AlignBasisTo(from : Basis, to : Basis) -> Basis:
	from.x = to.x.normalized() * from.x.length();
	from.y = to.y.normalized() * from.y.length();
	from.z = to.z.normalized() * from.z.length();
	return from 


# Using this y, return an orthonormal Basis with the given y direction.
func BasisFromYVector(y):
	y = y.normalized()
	var x = Vector3(1,0,0)
	if abs(y.dot(x))>.9: x = Vector3(0,1,0)
	var z = x.cross(y)
	x = y.cross(z)

	return Basis(x,y,z)

# Using this x, return an orthonormal Basis with the given x direction.
func BasisFromXVector(x):
	x = x.normalized()
	var y = Vector3(1,0,0)
	if abs(x.dot(y))>.9: y = Vector3(0,1,0)
	var z = x.cross(y)
	x = y.cross(z)

	return Basis(x,y,z)

# Using this z, return an orthonormal Basis with the given z direction.
func BasisFromZVector(z):
	z = z.normalized()
	var y = Vector3(1,0,0)
	if abs(z.dot(y))>.9: y = Vector3(0,1,0)
	var x = z.cross(y)
	y = z.cross(x)

	return Basis(x,y,z)


func RandomVector3() -> Vector3:
	return Vector3(2*randf()-1, 2*randf()-1, 2*randf()-1)


# Heron's formula for area of a triangle from the three side lengths.
func TriangleArea(l1 : float, l2 : float, l3 : float) -> float:
	var p = (l1 + l2 + l3) / 2
	return sqrt(p * (p - l1) * (p - l2) * (p - l3))


# Return [axis, angle] corresponding to q.
func ToAxisAngleRad(q : Quat):
	# out Vector3 axis, out float angle)
	if (abs(q.w) > 1.0):
		q = q.normalized()
	var angle = 2.0 * acos(q.w) # angle
	var den = sqrt(1.0 - q.w * q.w)
	var axis
	if (den > 0.0001):
		axis = Vector3(q.x, q.y, q.z) / den
	else:
		# This occurs when the angle is zero. 
		# Not a problem: just set an arbitrary normalized axis.
		axis = Vector3(1, 0, 0)
	return [axis,angle]
