extends Node


export(NodePath) var ViewportLabel

var viewport_label

func _ready():
	viewport_label = get_node(ViewportLabel)


func _on_Keyboard_KeyPressed(key):
	print ("Demo manager key pressed: ", key.key_value)
	if key.special_key == KEY_BACKSPACE || key.key_value == '\b':
		# special handling to delete chars...
		var txt : String = viewport_label.text
		if txt.length() > 0:
			txt = txt.substr(0,txt.length()-1)
			viewport_label.text = txt
			viewport_label.get_parent().get_parent().render_target_update_mode = Viewport.UPDATE_ONCE
		
	elif key.key_value == '\n':
		print ("Enter!")
		viewport_label.text = ""
		viewport_label.get_parent().get_parent().render_target_update_mode = Viewport.UPDATE_ONCE
		
	elif key.special_key == 0:
		# just add normal text
		viewport_label.text = viewport_label.text + key.key_value
		viewport_label.get_parent().get_parent().render_target_update_mode = Viewport.UPDATE_ONCE

